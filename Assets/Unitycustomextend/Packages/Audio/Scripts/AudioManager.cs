using System.Collections;
using System.Collections.Generic;
using UnityCustomExtension.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] int defaultAudioItemAmout;

    [SerializeField] AudioMixer mainMixer;

    [SerializeField] AudioItem audioItemPrefab;

    [SerializeField] List<AudioClip> SFXClips;

    [SerializeField] List<AudioClip> BGMusicClips;

    [SerializeField]
    [Range(0f, 1f)] float defaultSfxVolume = 1f;

    [SerializeField]
    [Range(0f, 1f)] float defaultBGMusicVolume = 1f;

    [SerializeField] AudioClip buttonClickAudio;

    public float SFXVolume
    {
        get
        {
            return PlayerPrefs.GetFloat(SFX_VOLUME_KEY, defaultSfxVolume);
        }
        set
        {
            SetMixerVolume(AudioVolumnParamName.SFX, value);
            PlayerPrefs.SetFloat(SFX_VOLUME_KEY, value);
        }
    }

    public float BGMusicVolume
    {
        get
        {
            return PlayerPrefs.GetFloat(BG_MUSIC_VOLUME_KEY, defaultBGMusicVolume);
        }
        set
        {
            SetMixerVolume(AudioVolumnParamName.BG_MUSIC, value);
            PlayerPrefs.SetFloat(BG_MUSIC_VOLUME_KEY, value);
        }
    }

    public bool IsSFXOn
    {
        get
        {
            return PlayerPrefsExtension.GetBool(IS_SFX_ON_KEY, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(IS_SFX_ON_KEY, value);
        }
    }
    public bool IsMusicOn
    {
        get
        {
            return PlayerPrefsExtension.GetBool(IS_MUSIC_ON_KEY, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(IS_MUSIC_ON_KEY, value);
        }
    }
    public bool IsAllOn
    {
        get
        {
            return PlayerPrefsExtension.GetBool(IS_ALL_ON_KEY, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(IS_ALL_ON_KEY, value);
        }
    }

    AudioItem bgAudioItem;

    private Queue<AudioItem> UnuseAudioItems = new Queue<AudioItem>();

    const string IS_SFX_ON_KEY = "IS_SFX_ON_KEY";

    const string IS_MUSIC_ON_KEY = "IS_MUSIC_ON_KEY";

    const string IS_ALL_ON_KEY = "IS_ALL_ON_KEY";

    const string BG_MUSIC_VOLUME_KEY = "BG_MUSIC_VOLUME_KEY";

    const string SFX_VOLUME_KEY = "SFX_VOLUME_KEY";

    int playingBGMusicIndex = -1;

    int playingBGMusicID = 0;

    Dictionary<int, SoundPlayer> soundPlayers = new Dictionary<int, SoundPlayer>();

    private void Start()
    {
        float sfxVolumeToSet = IsSFXOn ? SFXVolume : 0;
        float bgmVolumeToSet = IsMusicOn ? BGMusicVolume : 0;
        float allVolumeSet = IsAllOn ? 1 : 0;
        SetMixerVolume(AudioVolumnParamName.SFX, sfxVolumeToSet);
        SetMixerVolume(AudioVolumnParamName.BG_MUSIC, bgmVolumeToSet);
        SetMixerVolume(AudioVolumnParamName.MASTER, allVolumeSet);
        for (int i = 0; i < defaultAudioItemAmout; i++)
        {
            var audioItem = Instantiate(audioItemPrefab, transform);
            UnuseAudioItems.Enqueue(audioItem);
        }
    }

    void SetMixerVolume(string key, float value)
    {
        float volume = Mathf.Log10(value + 0.000001f) * 20;
        mainMixer.SetFloat(key, volume);
    }
    public bool TurnOnOffMusic()
    {
        IsSFXOn = !IsSFXOn;
        float sfxVolumnToSet = IsSFXOn ? SFXVolume : -80;
        mainMixer.SetFloat(AudioVolumnParamName.BG_MUSIC, sfxVolumnToSet);
        return IsSFXOn;
    }
    public bool TurnOnOffSFX()
    {
        IsSFXOn = !IsSFXOn;
        float sfxVolumnToSet = IsSFXOn ? SFXVolume : -80;
        mainMixer.SetFloat(AudioVolumnParamName.SFX, sfxVolumnToSet);
        return IsSFXOn;
    }
    public bool TurnOnOffAll()
    {
        IsAllOn = !IsAllOn;
        float sfxMusicToSet = IsAllOn ? BGMusicVolume : -80;
        mainMixer.SetFloat(AudioVolumnParamName.MASTER, sfxMusicToSet);
        return IsAllOn;
    }
    public void PlaySFX(int index)
    {
        if (index > SFXClips.Count - 1)
        {
            Debug.Log("Index out of clips count");
            return;
        }
        var audioItem = PlaySound(SFXClips[index], AudioMixerGroupType.SFX);
        audioItem.SetFinishAction(() =>
        {
            RecycleAudioItem(audioItem);
        });
    }
    public void PlaySFX(AudioClip clip)
    {
        var audioItem = PlaySound(clip, AudioMixerGroupType.SFX);
        audioItem.SetFinishAction(() =>
        {
            RecycleAudioItem(audioItem);
        });
    }

    SoundPlayer GetSoundPlayer(int id)
    {
        if (soundPlayers.ContainsKey(id))
        {
            return soundPlayers[id];
        }
        else
        {
            var soundPlayer = new SoundPlayer(0.1f);
            soundPlayers[id] = soundPlayer;
            return soundPlayer;
        }
    }
    public void PlayDelaySFX(AudioClip clip,float timeDelay)
    {
        GetSoundPlayer(clip.GetInstanceID()).PlaySound(clip).SetTimeDelay(timeDelay);
    }

    public void PlayButtonClickSFX()
    {
        PlaySound(buttonClickAudio, AudioMixerGroupType.SFX);
    }
    public void ChangeBGMusic(int index)
    {
        if (index > BGMusicClips.Count - 1)
        {
            Debug.Log("Index out of music clip count");
            return;
        }
        if (index == playingBGMusicIndex) return;
        playingBGMusicIndex = index;
        if (!bgAudioItem)
        {
            bgAudioItem = PlaySound(BGMusicClips[index], AudioMixerGroupType.BgMusic, true);
        }
        else
        {
            bgAudioItem.PlaySoundLoop(BGMusicClips[index]);
        }
    }
    public void ChangeBGMusic(AudioClip clip)
    {
        if (!clip) return;
        if (playingBGMusicID == clip.GetInstanceID()) return;
        playingBGMusicID = clip.GetInstanceID();
        if (!bgAudioItem)
        {
            bgAudioItem = PlaySound(clip, AudioMixerGroupType.BgMusic, true);
        }
        else
        {
            bgAudioItem.PlaySoundLoop(clip);
        }
    }
    public AudioItem PlaySound(AudioClip clip, AudioMixerGroupType mixerType, bool isLoop = false)
    {
        if (!clip)
        {
            return null;
        }
        var soundItem = GetAudioItem();
        if (isLoop)
        {
            soundItem.PlaySoundLoop(clip);
        }
        else
        {
            soundItem.PlaySoundOnce(clip);
        }
        soundItem.SetMixer(GetMixerGroup(mixerType));
        return soundItem;
    }
    public void PauseBGMusic()
    {
        if (bgAudioItem)
        {
            bgAudioItem?.PauseSound();
        }
    }
    public void UnPauseBGMusic()
    {
        if (bgAudioItem)
        {
            bgAudioItem?.UnPauseSound();
        }
    }

    AudioMixerGroup GetMixerGroup(AudioMixerGroupType mixerType)
    {
        switch (mixerType)
        {
            case AudioMixerGroupType.BgMusic:
                return mainMixer.FindMatchingGroups(AudioGroupName.BG_MUSIC)[0];
            case AudioMixerGroupType.SFX:
                return mainMixer.FindMatchingGroups(AudioGroupName.SFX)[0];
            case AudioMixerGroupType.Master:
                return mainMixer.FindMatchingGroups(AudioGroupName.MASTER)[0];
            default:
                return mainMixer.FindMatchingGroups(AudioGroupName.MASTER)[0];
        }
    }

    AudioItem GetAudioItem()
    {
        AudioItem audioItem = null;
        if (UnuseAudioItems.Count > 0)
        {
            audioItem = UnuseAudioItems.Dequeue();
        }
        else
        {
            audioItem = Instantiate(audioItemPrefab, transform);
        }
        return audioItem;
    }

    public void RecycleAudioItem(AudioItem item)
    {
        UnuseAudioItems.Enqueue(item);
        item.gameObject.SetActive(false);
    }
}

public enum AudioMixerGroupType
{
    Master,
    BgMusic,
    SFX
}

public static class AudioGroupName
{
    public const string MASTER = "Master";

    public const string BG_MUSIC = "Music";

    public const string SFX = "SFX";
}
public static class AudioVolumnParamName
{
    public const string MASTER = "MasterVolume";

    public const string SFX = "SfxVolume";

    public const string BG_MUSIC = "MusicVolume";
}