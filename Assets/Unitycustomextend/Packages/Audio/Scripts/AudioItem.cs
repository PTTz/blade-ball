using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

public class AudioItem : MonoBehaviour
{
    private AudioSource source;
    private AudioSource Source
    {
        get
        {
            if (source == null)
            {
                source = GetComponent<AudioSource>();
            }
            return source;
        }
    }

    private UnityAction finished;
    public bool IsPlaying
    {
        get
        {
            if (source != null)
            {
                return source.isPlaying;
            }
            return false;
        }
    }

    public void PlaySoundOnce(AudioClip audioClip)
    {
        if (!audioClip) return;
        gameObject.SetActive(true);
        StartCoroutine(PlaySoundCoroutine(audioClip));
    }

    IEnumerator PlaySoundCoroutine(AudioClip audioClip)
    {
        Source.loop = false;
        Source.clip = audioClip;
        Source.time = 0;
        Source.Play();
        float audioLength = audioClip.length;
        yield return new WaitUntil(() => Mathf.Abs(audioLength - Source.time) < 0.01f);
        finished?.Invoke();
        finished = null;
    }

    public void PlaySoundLoop(AudioClip audioClip)
    {
        gameObject.SetActive(true);
        Source.clip = audioClip;
        Source.loop = true;
        Source.Play();
    }
    public void Play()
    {
        gameObject.SetActive(true);
        Source.Play();
    }
    public void StopSound()
    {
        Source.Stop();
    }

    public void PauseSound()
    {
        Source.Pause();
    }

    public void UnPauseSound()
    {
        Source.UnPause();
    }

    public void SetFinishAction(UnityAction callback)
    {
        finished = callback;
    }

    public void Recyle()
    {
        AudioManager.Ins.RecycleAudioItem(this);
    }
    public void SetMixer(AudioMixerGroup mixerGroup)
    {
        Source.outputAudioMixerGroup = mixerGroup;
    }
}
