﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleData
{
    public int postId;
    public int id;
    public string name;
    public string email;
    public string body;
}
