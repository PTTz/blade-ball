using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityCustomExtension.UI;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;

// This script to turn on-off all ui for product
public class GUIBackUp : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] ActiveType TriTouchContinue;
    [SerializeField] List<Image> Images;
    [SerializeField] List<Text> Texts;
    [SerializeField] List<TextMeshProUGUI> TextMeshProUGUIs;
    [SerializeField] List<GameObject> ObjsToTurnOff;
    List<Color32> imgesb = new List<Color32>();
    List<Color32> textb = new List<Color32>();
    List<Color32> TMPb = new List<Color32>();
    bool isGUIActive = true;

    private void Start()
    {
        foreach (var item in Images)
        {
            imgesb.Add(item.color);
        }
        foreach (var item in Texts)
        {
            textb.Add(item.color);
        }
        foreach (var item in TextMeshProUGUIs)
        {
            TMPb.Add(item.color);
        }
    }
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        switch (TriTouchContinue)
        {
            case ActiveType.TriTouch:
                TriTouch_();
                break;
            case ActiveType.TriTouchContinue:
                TriTouchContinue_();
                break;
            default:
                break;
        }
    }
    void TriTouch_()
    {
        if (Input.touchCount == 3)
        {
            DOVirtual.Float(0, 1, 1, (x) => { }).OnComplete(() =>
            {
                if (isGUIActive)
                {
                    TurnOnAll();
                }
                else
                {
                    TurnOffAll();
                }
                //isGUIActive ? TurnOffAll() : TurnOnAll();
                isGUIActive = !isGUIActive;
            }).SetId($"{GetInstanceID()}GUIBackUp");
        }
    }
    int touchCounter = 0;
    void TriTouchContinue_()
    {
        touchCounter++;
        DOTween.Kill("TriTouchContinue_");
        DOVirtual.Float(0, 1, 1, (x) => { }).OnComplete(() =>
        {
            touchCounter = 0;
        }).SetId("TriTouchContinue_");
        if (touchCounter == 3)
        {
            if (isGUIActive)
            {
                TurnOnAll();
            }
            else
            {
                TurnOffAll();
            }
            //isGUIActive ? TurnOffAll() : TurnOnAll();
            isGUIActive = !isGUIActive;
        }
    }

    void TurnOffAll()
    {
        foreach (var item in ObjsToTurnOff)
        {
            item.SetActive(false);
        }
        foreach (var item in Images)
        {
            item.color = new Color32((byte)item.color.r, (byte)item.color.g, (byte)item.color.b, 0);
        }
        foreach (var item in Texts)
        {
            item.color = new Color32((byte)item.color.r, (byte)item.color.g, (byte)item.color.b, 0);
        }
        foreach (var item in TextMeshProUGUIs)
        {
            item.color = new Color32((byte)item.color.r, (byte)item.color.g, (byte)item.color.b, 0);
        }
    }
    void TurnOnAll()
    {
        foreach (var item in ObjsToTurnOff)
        {
            item.SetActive(true);
        }
        for (int i = 0; i < Images.Count; i++)
        {
            Images[i].color =  imgesb[i];
        }
        for (int i = 0; i < Texts.Count; i++)
        {
            Texts[i].color = textb[i];
        }
        for (int i = 0; i < TextMeshProUGUIs.Count; i++)
        {
            TextMeshProUGUIs[i].color = TMPb[i];
        }
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        DOTween.Kill($"{GetInstanceID()}GUIBackUp");
    }
}
public enum ActiveType
{
    TriTouch, TriTouchContinue
}
