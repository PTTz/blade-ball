using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public enum TimerType
{
    hour,
    minute,
    second
}

[RequireComponent(typeof(Text))]
public class Timer : MonoBehaviour
{
    [SerializeField] private TimerType type;
    Text countDownText;

    IEnumerator ieCountDown;

    bool isStop = false;
    int currentSecond;
    public int CurrentSecond { get { return currentSecond; } }
    private void Awake()
    {
        countDownText = GetComponent<Text>();
    }

    public void SetType(TimerType type)
    {
        this.type = type;
    }
    public void CountDown(int second, UnityAction finish = null)
    {
        currentSecond = second;
        if (ieCountDown != null) TimerController.Instance.StopCoroutine(ieCountDown);
        ieCountDown = IEHourCountDown(finish);
        TimerController.Instance.StartCoroutine(ieCountDown);
    }
    IEnumerator IEHourCountDown(UnityAction finish = null)
    {

        while (currentSecond > 0) // run until second = 0
        {
            countDownText.text = GetCountDownFormat(currentSecond, type);
            currentSecond--;
            yield return new WaitForSecondsRealtime(1);
            yield return new WaitUntil(() => !isStop);
        }
        finish?.Invoke();
    }
    public void StopCountDown(bool completely = false)
    {
        if (completely)
        {
            if (ieCountDown != null)
            {
                StopCoroutine(ieCountDown);
            }
        }
        else
        {
            isStop = true;
        }
    }
    public void ContinueCountDown()
    {
        isStop = false;
    }
    public string GetCountDownFormat(int second, TimerType type = TimerType.second)
    {
        switch (type)
        {
            case TimerType.hour:
                return GetHourFormat(second);
            case TimerType.minute:
                return GetMinuteFormat(second);
            default:
                return second.ToString();
        }
    }
    // 1 minute = 60 second
    // 1 hour = 60 minute = 60*60 second;
    public string GetHourFormat(int second)
    {
        int hour = second / (60 * 60);
        int minute = second % (60 * 60) / 60;
        second = second % (60 * 60) % 60;
        string hourFormat = hour < 10 ? "0" + hour : hour.ToString();
        string minuteFormat = minute < 10 ? "0" + minute : minute.ToString();
        string secondFormat = second < 10 ? "0" + second : second.ToString();
        return hourFormat + ":" + minuteFormat + ":" + secondFormat;
    }
    public string GetMinuteFormat(int second)
    {
        int minute = second / 60;
        second = second % 60;
        string minuteFormat = minute < 10 ? "0" + minute : minute.ToString();
        string secondFormat = second < 10 ? "0" + second : second.ToString();
        return minuteFormat + ":" + secondFormat;
    }

    public string GetSecondFormat(int second)
    {
        return second < 10 ? "0" + second : second.ToString();
    }
}
