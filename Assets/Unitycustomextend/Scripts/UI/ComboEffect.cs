using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboEffect : MonoBehaviour
{
    [SerializeField] Image comboImage;
    [SerializeField] Text comboText;
    [SerializeField] float scale;
    [SerializeField] float timeScale;
    [SerializeField] float timeShow;
    [SerializeField] List<Sprite> comboSprites;
    [SerializeField] List<Color> comboColors;
    Tween tween;
    IEnumerator ieShow = null;
    private void Start()
    {
        gameObject.SetActive(false);
    }
    public void ShowCombo(int comboCount)
    {
        gameObject.SetActive(true);
        comboImage.sprite = GetComboSprite(comboCount);
        comboText.text = $"x{comboCount}";
        comboText.color = GetComboColor(comboCount);
        if (ieShow != null)
        {
            StopCoroutine(ieShow);
        }
        tween?.Kill();
        ieShow = IEShowCombo();
        StartCoroutine(ieShow);
    }

    Sprite GetComboSprite(int combo)
    {
        if (combo < 50) return comboSprites[0];
        if (combo >= 50 && combo < 100) return comboSprites[1];
        if (combo >= 100 && combo < 150) return comboSprites[2];
        if (combo >= 150 && combo < 300) return comboSprites[3];
        return comboSprites[4];
    }
    Color GetComboColor(int combo)
    {
        if (combo < 50) return comboColors[0];
        if (combo >= 50 && combo < 100) return comboColors[1];
        if (combo >= 100 && combo < 150) return comboColors[2];
        if (combo >= 150 && combo < 300) return comboColors[3];
        return comboColors[4];
    }
    IEnumerator IEShowCombo()
    {
        gameObject.transform.localScale = Vector3.one;
        tween = transform.DOScale(Vector3.one * scale, timeScale).SetUpdate(true);
        yield return new WaitForSeconds(timeScale);
        tween = transform.DOScale(Vector3.one, timeScale).SetUpdate(true);
        yield return new WaitForSeconds(timeScale);
        yield return new WaitForSeconds(timeShow);
        gameObject.SetActive(false);
    }
}
