using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class AntiSpamBtn : MonoBehaviour
{
    [SerializeField] int delayTime;
    [SerializeField] List<Button> buttons;
    Button Btn;
    private void Start()
    {
        Btn = GetComponent<Button>();
        Btn.onClick.AddListener(OnClick);
    }
    async void OnClick()
    {
        Btn.interactable = false;
        foreach (var btn in buttons)
        {
            btn.interactable = false;
        }
        await Task.Delay(delayTime);
        Btn.interactable = true;
        foreach (var btn in buttons)
        {
            btn.interactable = true;
        }
    }
}
