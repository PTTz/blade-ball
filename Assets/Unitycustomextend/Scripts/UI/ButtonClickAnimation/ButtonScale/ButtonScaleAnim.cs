using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScaleAnim : MonoBehaviour
{
    Button btn;
    Animator animator;
    bool Pressing = false;
    private void Start()
    {
        btn = GetComponent<Button>();
        animator = GetComponent<Animator>();
        //btn.onClick.AddListener(OnClick);
    }
    public void OnPress()
    {
        if (Pressing) return;
        Pressing = true;
        animator.SetBool("Pressing", true);
        //Debug.Log("OnPress");
    }
    public void OnNormal()
    {
        Pressing = false;
        animator.SetBool("Pressing", false);
        //Debug.Log("OnNormal");
    }
}
