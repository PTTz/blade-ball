using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UnityCustomExtension.Animation
{
    public static class AnimHelper
    {
        public static void FadeAll(this GameObject obj, float originalAlpha, float targetAlpha, float time = 0.6f)
        {
            DOTween.Kill(obj);
            obj.Fade(originalAlpha, targetAlpha, time);
            for (int i = 0; i < obj.transform.childCount; i++)
            {
                var item = obj.transform.GetChild(i);
                item.gameObject.Fade(originalAlpha, targetAlpha, time);
            }
        }
        public static void Fade(this GameObject obj, float originalAlpha, float targetAlpha, float time = 0.6f)
        {
            if (obj.name.Contains("non_fade")) return;
            var text = obj.GetComponent<Text>();
            if (text)
            {
                var color = text.color;
                color.a = originalAlpha;
                text.color = color;
                text.DOFade(targetAlpha, time).SetEase(Ease.Linear);
            }
            var outline = obj.GetComponent<Outline>();
            if (outline)
            {
                var color = outline.effectColor;
                color.a = originalAlpha;
                outline.effectColor = color;
                outline.DOFade(targetAlpha, time).SetEase(Ease.Linear);
            }
            var shadow = obj.GetComponent<Outline>();
            if (shadow)
            {
                var color = shadow.effectColor;
                color.a = originalAlpha;
                shadow.effectColor = color;
                shadow.DOFade(targetAlpha, time).SetEase(Ease.Linear);
            }
            var img = obj.GetComponent<Image>();
            if (img)
            {
                var color = img.color;
                color.a = originalAlpha;
                img.color = color;
                img.DOFade(targetAlpha, time).SetEase(Ease.Linear);
            }
        }
    }
}