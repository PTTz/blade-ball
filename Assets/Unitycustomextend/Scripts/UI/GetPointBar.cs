using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityCustomExtension.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GetPointBar : MonoBehaviour
{
    [SerializeField] Transform icon;

    [SerializeField] RecylableObject iconPrefab;

    [SerializeField] float jumpPower = 50f;

    [SerializeField] float jumpRange = 100f;

    Sequence iconSeq;

    float timeDelay = 0.05f;

    public void PlayGetPointEffect(Vector3 pos, int quantities, UnityAction stepFinish = null, UnityAction finish = null)
    {
        StartCoroutine(IEPlayGetPointEffect(pos, quantities, stepFinish, finish));
    }
    IEnumerator IEPlayGetPointEffect(Vector3 pos, int quantities, UnityAction stepFinish = null, UnityAction finish = null)
    {
        int numWait = 0;
        int numPlay = quantities;
        for (int i = 0; i < numPlay; i++)
        {
            var item = PoolObjects.Ins.SpawnObject(iconPrefab);
            item.gameObject.SetActive(true);
            item.transform.SetParent(transform);
            item.transform.localScale = Vector3.one;
            var position = pos;
            position.z = transform.position.z;
            item.transform.position = position;
            Vector3 JumpPos = item.transform.position.GetRandomInRange(jumpRange, jumpRange, 0);
            item.gameObject.SetActive(true);
            numWait++;
            int index = i;
            item.transform.DOJump(JumpPos, jumpPower, 1, Random.Range(0.4f, 0.6f)).OnComplete(() =>
            {
                item.transform.DOMove(icon.position, Random.Range(0.5f, 0.8f)).OnComplete(() =>
                {
                    stepFinish?.Invoke();
                    PoolObjects.Ins.DestroyObject(item);
                    iconSeq?.Kill();
                    iconSeq = DOTween.Sequence();
                    icon.localScale = Vector3.one;
                    iconSeq.Insert(0, icon.DOScale(Vector3.one * 1.3f, 0.1f));
                    iconSeq.Insert(0.1f, icon.DOScale(Vector3.one, 0.2f));
                    iconSeq.SetUpdate(true);
                    numWait--;
                }).SetUpdate(true);
            }).SetUpdate(true);
            yield return new WaitForSecondsRealtime(timeDelay);
        }
        yield return new WaitUntil(() => numWait <= 0);
        finish?.Invoke();
    }

}
