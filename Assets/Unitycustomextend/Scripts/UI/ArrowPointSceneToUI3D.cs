using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using UnityCustomExtension.Generic;
using System.Collections.Generic;

// Canvas must be in "Scene Space - Overlay" render mode
public class ArrowPointSceneToUI3D : RecylableObject
{
    [Header("Status")]
    [SerializeField] bool Display;
    [SerializeField] bool IsOutOfView;
    [SerializeField] bool updateDistance;
    [SerializeField] OutOfViewType outOfViewType;
    [SerializeField] float Angle;
    [SerializeField] Vector3 PointerUIPos;

    [Header("SetUp")]
    [SerializeField] bool AutoDisable;
    [SerializeField] float AutoDisableDistance;
    [SerializeField] Sprite ArrowOnSceneSprite;
    [SerializeField] GameObject SubArrowOnSceneSprite;
    [SerializeField] Sprite ArrowOffSceneSprite;
    [SerializeField] Transform Player;
    [SerializeField] Transform Target;
    [SerializeField] Camera Cam;
    [SerializeField] List<Color> ColorList;
    [SerializeField] int colorIndex;
    [SerializeField] float BorderSize = 100;
    [SerializeField] Vector3 Offset;

    [Header("Referent")]
    [SerializeField] TextMeshProUGUI DistanceTMP;
    [SerializeField] Transform UICenterPoint;
    [SerializeField] Image PointerImg;
    [SerializeField] Transform PointerTranform;

    int currentDistance;

    private void Update()
    {
        if (Target == null || Player == null || Cam == null) return;
        PointerUIPos = Cam.WorldToScreenPoint(Target.position);

        UpdateDistance();
        UpdatePointerAngle();

        if (IsOutOfView)
        {
            UpdateTargetOutOfViewZone();
        }
        else
        {
            UpdateTargetOnViewZone();
        }
        UpdateAutoDisable();
        if (PointerUIPos.z < 0)
        {
            return;
        }
        SetSpriteAndoutOfView(PointerUIPos.x <= BorderSize - Offset.x
            || PointerUIPos.x >= Screen.width - BorderSize - Offset.x
            || PointerUIPos.y <= BorderSize - Offset.y
            || PointerUIPos.y >= Screen.height - BorderSize - Offset.y);
    }
    public void SetUp(Transform player, Transform target, Camera cam, int ColorIndex, float autoDisableDistance)
    {
        Player = player;
        Target = target;
        Cam = cam;
        colorIndex = ColorIndex;
        PointerImg.color = ColorList[ColorIndex];
        AutoDisableDistance = autoDisableDistance;
    }
    public void SetUpOnSceneSprite(Sprite newSprite)
    {
        ArrowOnSceneSprite = newSprite;
    }
    public void SetUpOffSceneSprite(Sprite newSprite)
    {
        ArrowOffSceneSprite = newSprite;
    }
    public void SetDisplay(bool isDisplay)
    {
        PointerTranform.gameObject.SetActive(isDisplay);
    }
    void UpdateDistance()
    {
        if (!updateDistance) return;
        currentDistance = (int)(Vector3.Distance(Target.position, Player.position));
        DistanceTMP.text = $"{currentDistance}m";
    }
    void UpdateAutoDisable()
    {
        if (!AutoDisable) return;
        if (Vector3.Distance(Target.position, Player.position) <= AutoDisableDistance)
        {
            Recycle();
        }
    }
    void UpdatePointerAngle()
    {
        if (IsOutOfView)
        {
            Vector3 dir = (new Vector3(PointerImg.transform.position.x, PointerImg.transform.position.y, 0) - UICenterPoint.position).normalized;
            Angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) % 360;
            PointerImg.transform.localEulerAngles = new Vector3(0, 0, Angle - 90);
        }
        else
        {
            PointerImg.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
    void UpdateTargetOutOfViewZone()
    {
        if (PointerUIPos.x <= BorderSize) PointerUIPos.x = BorderSize - Offset.x;
        if (PointerUIPos.x >= Screen.width - BorderSize) PointerUIPos.x = Screen.width - BorderSize - Offset.x;
        if (PointerUIPos.y <= BorderSize) PointerUIPos.y = BorderSize - Offset.y;
        if (PointerUIPos.y >= Screen.height - BorderSize) PointerUIPos.y = Screen.height - BorderSize - Offset.y;
        UpdateTargetOnViewZone();
    }
    void UpdateTargetOnViewZone()
    {
        if (PointerUIPos.z < 0)
        {
            SetSpriteAndoutOfView(true);// dang sau quay ???
            Vector3 v = new Vector3(PointerTranform.position.x, PointerUIPos.y, 0);
            v += Offset;
            PointerTranform.DOMove(v, 0.1f);
        }
        else
        {
            SetSpriteAndoutOfView(false);
            PointerTranform.DOMove(PointerUIPos + Offset, 0.1f);
        }
    }
    void SetSpriteAndoutOfView(bool input)
    {
        if (input == IsOutOfView) return;
        IsOutOfView = input;
        if (IsOutOfView)
        {
            switch (outOfViewType)
            {
                case OutOfViewType.TurnOff:
                    PointerImg.gameObject.SetActive(false);
                    break;
                case OutOfViewType.ChangeToArrow:
                    PointerImg.sprite = ArrowOffSceneSprite;
                    PointerImg.color = ColorList[colorIndex];
                    break;
                default:
                    break;
            }
            SubArrowOnSceneSprite.SetActive(false);
        }
        else
        {
            PointerImg.gameObject.SetActive(true);
            PointerImg.sprite = ArrowOnSceneSprite;
            SubArrowOnSceneSprite.SetActive(true);
            PointerImg.color = ColorList[0];
        }
    }
}
public enum OutOfViewType
{
    ChangeToArrow, TurnOff
}
