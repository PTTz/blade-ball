﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityCustomExtension.UI
{
    public class GUIController : Singleton<GUIController>
    {
        [Tooltip("GUI which already on scene on editor mode and Active at start ( anti lag )")]
        [SerializeField]
        List<BaseGUI> PreLoadGUISActive;

        [Tooltip("GUI which already on scene on editor mode and Deactive at start ( anti lag )")]
        [SerializeField]
        List<BaseGUI> PreLoadGUISDeactive;

        [SerializeField, DisableInEditorMode] private Dictionary<string, BaseGUI> listGuis = new Dictionary<string, BaseGUI>();

        [SerializeField, DisableInEditorMode] private List<BaseGUI> _listPopupGUI;

        Camera mainCamera;

        [SerializeField] private string UIPath = "GUI/";

        [HideInEditorMode] public Action<BaseGUI> OnOpenGUI;

        [HideInEditorMode] public Action<BaseGUI> OnCloseGUI;

        [HideInEditorMode] public Action OnPopupOpen;

        [HideInEditorMode] public Action OnPopupAllClose;

        protected override void SetUp()
        {
            base.SetUp();

            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            OnChangeScene();

            OnCloseGUI += OnCloseGUIFuntion;
        }

        private void OnCloseGUIFuntion(BaseGUI gui)
        {
            if (_listPopupGUI.Count == 0) return;

            if (_listPopupGUI.Contains(gui))
            {
                _listPopupGUI.Remove(gui);
            }

            if(_listPopupGUI.Count == 0)
            {
                OnPopupAllClose?.Invoke();
            }
        }

        private void OnChangeScene()
        {
            mainCamera = Camera.main;

            foreach (var item in listGuis)
            {
                item.Value.MainCanvas.worldCamera = mainCamera;
            }
        }

        public override void Start()
        {
            base.Start();

            foreach (var gui in PreLoadGUISActive)
            {
                Log($"GUIController -> Start -> add {gui.name} to gui list");

                listGuis.Add(gui.name, gui);
                gui.gameObject.SetActive(true);
                gui.IsOpenning = true;
            }

            foreach (var gui in PreLoadGUISDeactive)
            {
                Log($"GUIController -> Start -> add {gui.name} to gui list");
                listGuis.Add(gui.name, gui);
                gui.gameObject.SetActive(false);
                gui.IsOpenning = false;
            }
        }

        public void HideAllGUI()
        {
            foreach (var item in listGuis)
            {
                item.Value.Close();
            }
        }

        public void TemporaryHideAllGUI_Except(string guiName)
        {
            foreach (var gui in listGuis)
            {
                if (gui.Value.name != guiName)
                {
                    gui.Value.gameObject.SetActive(false);
                }
            }
        }

        public void TemporaryHideAllGUI_Except(string[] guiNameArr)
        {
            foreach (var gui in listGuis.Where(gui => !guiNameArr.Contains(gui.Value.name)))
            {
                gui.Value.gameObject.SetActive(false);
            }
        }

        public void ShowAllGUITemporaryHide()
        {
            foreach (var gui in listGuis)
            {
                if (gui.Value.IsOpenning)
                {
                    gui.Value.gameObject.SetActive(true);
                }
            }
        }

        public void ShowAllGUITemporaryHide_Except(string guiName)
        {
            foreach (var gui in listGuis)
            {
                if (gui.Value.IsOpenning && gui.Value.name != guiName)
                {
                    gui.Value.gameObject.SetActive(true);
                }
            }
        }

        public void CloseGUI(string guiName)
        {
            if (GetGUI(guiName, out var baseGUI))
            {
                baseGUI.Close();
            }
        }

        public bool GetGUI(string guiName, out BaseGUI baseGUI)
        {
            if (listGuis.TryGetValue(guiName, out var gui))
            {
                baseGUI = gui;
                return true;
            }

            try
            {
                var prefabPath = $"{UIPath}{guiName}";
                var prefabGui = Resources.Load<BaseGUI>(prefabPath);
                baseGUI = Instantiate(prefabGui, transform);
                baseGUI.name = guiName;
                baseGUI.MainCanvas.worldCamera = mainCamera;
                Log($"GUIController -> Start -> add {guiName} to gui list");
                listGuis.Add(guiName, baseGUI);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                baseGUI = null;
                return false;
            }
        }

        public T GetGUIAs<T>(string guiName)
        {
            if (GetGUI(guiName, out var baseGUI))
            {
                return baseGUI.GetComponent<T>();
            }

            return default;
        }

        public void ShowGUI(string guiName)
        {
            if (GetGUI(guiName, out var baseGUI))
            {
                Log($"GUIController -> ShowGUI {guiName}");

                baseGUI.Open(null);

                if (baseGUI.isPopup && !_listPopupGUI.Contains(baseGUI))
                {
                    _listPopupGUI.Add(baseGUI);

                    OnPopupOpen?.Invoke();
                }
            }
            else
            {
                Debug.LogError($"<color=black>[GuiController]</color> Not exist GUI: {guiName}");
            }
        }

        public T ShowGUIAs<T>(string guiName)
        {
            if (GetGUI(guiName, out var baseGUI))
            {
                Log($"GUIController -> ShowGUI {guiName}");

                baseGUI.Open(null);

                if (baseGUI.isPopup && !_listPopupGUI.Contains(baseGUI))
                {
                    _listPopupGUI.Add(baseGUI);

                    OnPopupOpen?.Invoke();
                }
            }
            else
            {
                Debug.LogError($"<color=black>[GuiController]</color> Not exist GUI: {guiName}");
            }

            return baseGUI.GetComponent<T>();
        }

        public void HideGUI(string guiName)
        {
            if (GetGUI(guiName, out var baseGUI))
            {
                baseGUI.Close();
            }
            else
            {
                Debug.LogWarning($"<color=black>[GuiController]</color> Not exist GUI: {guiName}");
            }
        }

        public bool GetStatusOfGUI(string guiName)
        {
            if (listGuis.TryGetValue(guiName, out var gui))
            {
                return gui.IsOpenning;
            }

            return false;
        }
    }
}