using System.Collections;
using System.Collections.Generic;
using UnityCustomExtension.UI;
using UnityEngine;
using UnityEngine.Events;

public class BaseGUIScreenSpaceCamera : BaseGUI
{
    public bool FixedDistanceScreenSpaceUI;
    public float DistanceToCamera;

    public override void Close(UnityAction finish = null)
    {
        base.Close(finish);
        if (MainCanvas.renderMode == RenderMode.ScreenSpaceCamera && FixedDistanceScreenSpaceUI)
        {
            MainCanvas.planeDistance = DistanceToCamera;
        }
    }
}
