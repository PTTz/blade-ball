using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UnityCustomExtension.Animation
{
    public class GUIAnim : MonoBehaviour
    {
        public virtual void Open(UnityAction finish = null)
        {

        }
        public virtual void Close(UnityAction finish = null)
        {

        }
    }

}