using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGUISingleton<T> : BaseGUI where T : BaseGUISingleton<T>
{

    private static T instance;

    public static T Ins => instance;
    [SerializeField] private bool needDontDestroy = false;

    public virtual void Awake()
    {
        if (instance == null)
        {
            instance = (T)this;
            if (needDontDestroy) DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        SetUp();
    }

    private void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
}