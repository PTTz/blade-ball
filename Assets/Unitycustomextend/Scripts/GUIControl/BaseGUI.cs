using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using UnityCustomExtension.UI;

public abstract class BaseGUI : ExtendMonoBehavior
{
    public int GUI_Layer;

    public Button btnClose;

    public Button[] otherBtnClose = new Button[0];

    Canvas mainCanvas;

    [DisableInEditorMode] public bool IsOpenning;

    [SerializeField] public bool isPopup;

    public Canvas MainCanvas
    {
        get
        {
            if (!mainCanvas)
            {
                mainCanvas = GetComponent<Canvas>();
            }
            return mainCanvas;
        }
    }
    public virtual void Awake()
    {
        SetUp();

        btnClose?.onClick.AddListener(OnClickBtnClose);

        if (otherBtnClose.Length > 0)
        {
            foreach (var btn in otherBtnClose)
            {
                btn.onClick.AddListener(OnClickBtnClose);
            }
        }
    }

    [ContextMenu("Open GUI")]
    public void FakeOpen()
    {
        Open(null, null);
    }

    public virtual void Open(object[] initParams, UnityAction finish = null)
    {
        if (IsOpenning) return;
        IsOpenning = true;
        gameObject.SetActive(true);

    }

    public virtual void Close(UnityAction finish = null)
    {
        gameObject.SetActive(false);

        IsOpenning = false;

        GUIController.Ins.OnCloseGUI?.Invoke(this);
    }

    public virtual void SetUp()
    {

    }

    [ContextMenu("Click Close")]
    public virtual void OnClickBtnClose()
    {
        Close();
    }
}