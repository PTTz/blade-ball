using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCustomExtension.UI;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Threading.Tasks;

public class FakeAdsUI : BaseGUI
{
    [SerializeField] Button ExitBtn;
    public UnityAction OnFakeAdsDone;
    private void Start()
    {
        ExitBtn.onClick.AddListener(OnClickExit);
    }
    public override void Open(object[] initParams, UnityAction finish = null)
    {
        base.Open(initParams, finish);
        ExitBtn.gameObject.SetActive(false);
        WaitForBack();
    }
    async void WaitForBack()
    {
        await Task.Delay(1000);
        ExitBtn.gameObject.SetActive(true);
    }
    void OnClickExit()
    {
        OnFakeAdsDone?.Invoke();
        OnFakeAdsDone = null;
        Close();
    }
}
