using UnityEngine;
using UnityEngine.UI;
using UnityCustomExtension.UI;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class BaseGUI_ScaleAnim : BaseGUI
{
    public Transform TableOfContent;
    [Header("Scale")]
    public AnimationCurve curve;
    public Vector3 startScale = new Vector3(0, 0, 0);
    public Vector3 startScaleIn = new Vector3(1.3f, 1.3f, 1.3f);
    public Vector3 startScaleOut = new Vector3(1, 1, 1);
    [Header("Open")]
    public float animOpenTimeIn = 0.3f;
    public float animOpenTimeOut = 0.15f;
    [Header("Close")]
    public float animCloseTimeIn = 0.3f;
    public float animCloseTimeOut = 0.15f;

    public Action OnTableOfContentOpenEnd;

    public override void Open(object[] initParams, UnityAction finish = null)
    {
        base.Open(initParams, finish);
        TableOfContent.localScale = startScale;
        TableOfContent.DOScale(startScaleIn, animOpenTimeIn).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOScale(startScaleOut, animOpenTimeOut).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}").OnComplete(() =>
            {
                OnTableOfContentOpenEndFuntion();

                OnTableOfContentOpenEnd?.Invoke();
            });
        });
    }

    public virtual void OnTableOfContentOpenEndFuntion()
    {

    }

    public override void Close(UnityAction finish = null)
    {
        IsOpenning = false;
        DOTween.Kill($"OpenPopupOwnItemUI{GetInstanceID()}");
        DOTween.Kill($"OpenPopupOwnItemUI2{GetInstanceID()}");
        TableOfContent.DOScale(startScaleIn, animCloseTimeIn).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOScale(startScale, animCloseTimeOut).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}").OnComplete(() =>
            {
                base.Close(finish);
            });
        });
    }
}
