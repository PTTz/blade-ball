using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using UnityCustomExtension.UI;
using UnityEngine;
using UnityEngine.Events;

public class BaseGUI_StretchAnim : BaseGUI
{
    [Header("Anim")]
    [SerializeField] Transform BG;
    public AnimationCurve BGCurve;
    [SerializeField] float BGEndScaleX = 20;
    [SerializeField] float TimeSaleBGUp = 0.3f;
    [SerializeField] float TimeSaleBGDown = 0.15f;
    [SerializeField] int DelayUntilShowToC = 200;
    [SerializeField] int DelayUntilHideToC = 450;
    [SerializeField] Transform TableOfContent;
    [Header("Scale")]
    public AnimationCurve ToCCurve;
    public Vector3 startScale = new Vector3(0, 0, 0);
    public Vector3 startScaleIn = new Vector3(1.3f, 1.3f, 1.3f);
    public Vector3 startScaleOut = new Vector3(1, 1, 1);
    [Header("Open")]
    public float animOpenTimeIn = 0.3f;
    public float animOpenTimeOut = 0.15f;
    [Header("Close")]
    public float animCloseTimeIn = 0.3f;
    public float animCloseTimeOut = 0.15f;
    private async void Start()
    {
        Open(null);
        await Task.Delay(3000);
        Close();
    }

    public async override void Open(object[] initParams, UnityAction finish = null)
    {
        base.Open(initParams, finish);
        BG.localScale = new Vector3(0, BG.localScale.y, BG.localScale.z);
        TableOfContent.localScale = startScale;

        BG.DOScaleX(BGEndScaleX, TimeSaleBGUp).SetEase(BGCurve).SetUpdate(true);
        await Task.Delay(DelayUntilShowToC);
        TableOfContent.DOScale(startScaleIn, animOpenTimeIn).SetEase(ToCCurve).SetUpdate(true).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOScale(startScaleOut, animOpenTimeOut).SetEase(ToCCurve).SetUpdate(true).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}").OnComplete(() =>
            {
                OnCompletedOpenAnim();
            });
        });
    }
    public virtual void OnCompletedOpenAnim()
    {

    }
    public async override void Close(UnityAction finish = null)
    {
        DOTween.Kill($"OpenPopupOwnItemUI{GetInstanceID()}");
        DOTween.Kill($"OpenPopupOwnItemUI2{GetInstanceID()}");
        TableOfContent.DOScale(startScaleIn, animCloseTimeIn).SetEase(ToCCurve).SetUpdate(true).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOScale(startScale, animCloseTimeOut).SetEase(ToCCurve).SetUpdate(true).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}");
        });
        await Task.Delay(DelayUntilHideToC);
        BG.DOScaleX(0, TimeSaleBGDown).SetEase(BGCurve).OnComplete(() =>
        {
            base.Close(finish);
        }).SetUpdate(true);
    }
}
