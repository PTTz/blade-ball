using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCustomExtension.UI;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class BaseGUI_BlendColorAnim : BaseGUI
{
    [SerializeField] Image BgIN;
    [SerializeField] Image BgOUT;
    [SerializeField] Color32 EndColor;
    [SerializeField] float TimeIN;
    [SerializeField] float TimeOUT;
    [SerializeField] GameObject TableOfContent;
    public virtual void Start()
    {
        BgIN.color = new Color32(0, 0, 0, 0);
        BgOUT.gameObject.SetActive(false);
        TableOfContent.gameObject.SetActive(false);
    }
    public override void Open(object[] initParams, UnityAction finish = null)
    {
        BgIN.DOColor(EndColor, TimeIN).OnComplete(() =>
        {
            OpenBlendComplete();
            BgOUT.gameObject.SetActive(true);
            BgOUT.DOColor(new Color32(0, 0, 0, 0), TimeOUT).OnComplete(() =>
            {
                base.Open(initParams, finish);

            });
        });
    }
    public virtual void OpenBlendComplete()
    {
        TableOfContent.gameObject.SetActive(true);
    }
    public override void Close(UnityAction finish = null)
    {
        BgIN.color = new Color32(0, 0, 0, 0);
        base.Close(finish);
    }
}
