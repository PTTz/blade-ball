using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityCustomExtension.UI;
using UnityEngine;
using UnityEngine.Events;

public class BaseGUI_DropDownAnim : BaseGUI
{
    public Transform TableOfContent;
    [Header("Drop")]
    public AnimationCurve curve;
    public Transform StartPos;
    private Vector3 EndPos;
    public Vector3 Offset = new Vector3(0, 100, 0);
    [Header("Open")]
    public float animOpenTimeIn = 0.3f;
    public float animOpenTimeOut = 0.15f;
    [Header("Close")]
    public float animCloseTimeIn = 0.3f;
    public float animCloseTimeOut = 0.15f;

    public override void SetUp()
    {
        base.SetUp();
        EndPos = TableOfContent.transform.position;
        TableOfContent.transform.position = StartPos.transform.position;
    }

    public override void Open(object[] initParams, UnityAction finish = null)
    {
        base.Open(initParams, finish);
        TableOfContent.position = StartPos.position;
        TableOfContent.DOMove(EndPos - Offset, animOpenTimeIn).SetUpdate(true).SetEase(curve).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOMove(EndPos, animOpenTimeOut).SetUpdate(true).SetEase(curve).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}");
        });
    }
    public override void Close(UnityAction finish = null)
    {
        DOTween.Kill($"OpenPopupOwnItemUI{GetInstanceID()}");
        DOTween.Kill($"OpenPopupOwnItemUI2{GetInstanceID()}");
        TableOfContent.DOMove(EndPos - Offset, animCloseTimeIn).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI{GetInstanceID()}").OnComplete(() =>
        {
            TableOfContent.DOMove(StartPos.position, animCloseTimeOut).SetEase(curve).SetUpdate(true).SetId($"OpenPopupOwnItemUI2{GetInstanceID()}").OnComplete(() =>
            {
                base.Close(finish);
                OnCloseDone();
            });
        });

    }
    public virtual void OnCloseDone()
    {

    }
}
