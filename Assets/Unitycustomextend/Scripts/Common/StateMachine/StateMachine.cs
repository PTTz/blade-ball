using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T>
{
    public bool IsInitilized { get; private set; }
    public State<T> CurrentState { get; private set; }

    public void Initialize(State<T> startingState)
    {
        IsInitilized = true;
        CurrentState = startingState;
        startingState.EnterState();
    }

    public void ChangeState(State<T> newState)
    {
        if (newState == CurrentState) return;
        //Debug.Log($"<color=blue>Change state from {CurrentState.GetType()} to {newState.GetType()}</color>");
        CurrentState.ExitState();
        CurrentState = newState;
        newState.EnterState();
    }
}
