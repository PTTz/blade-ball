using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State<T>
{
    protected T stateController;

    protected StateMachine<T> stateMachine;

    protected State(T controller, StateMachine<T> stateMachine)
    {
        this.stateController = controller;
        this.stateMachine = stateMachine;
    }

    public virtual void EnterState()
    {
    }

    public virtual void HandleInput()
    {

    }

    public virtual void StateUpdate()
    {

    }

    public virtual void StateFixedUpdate()
    {

    }
    public virtual void StateLateUpdate()
    {

    }

    public virtual void ExitState()
    {

    }
}
