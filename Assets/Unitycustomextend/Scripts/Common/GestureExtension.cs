using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityCustomExtension.Generic
{
    public static class GestureExtension
    {
        public static Vector3 TouchPosition
        {
            get
            {
#if UNITY_EDITOR
                return Input.mousePosition;
#elif UNITY_ANDROID||UNITY_IOS
                return Input.GetTouch(0).position;
#endif
            }
        }
        public static bool IsMouseOverGameObject()
        {
#if UNITY_EDITOR
        return EventSystem.current.IsPointerOverGameObject();
#elif UNITY_ANDROID || UNITY_IOS
            return EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#endif
        }
    }
}