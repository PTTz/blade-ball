using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoInstantiateSingleton<T> : MonoBehaviour where T : AutoInstantiateSingleton<T>
{
    private static T instance;

    private void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
    protected virtual void SetUp()
    {

    }
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject gObject = new GameObject(string.Format("(Singleton) {0}", typeof(T).Name));
                instance = gObject.AddComponent<T>();
                DontDestroyOnLoad(gObject);
            }
            return instance;
        }
    }
}
