using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjectsSort : MonoBehaviour
{
    [SerializeField] float distanceBetweenList;

    [SerializeField] bool isHorizontal;
    public void Sort()
    {
        transform.SortChild(distanceBetweenList, isHorizontal);
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(ObjectsSort))]
public class OrderModels_Inspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var script = (ObjectsSort)target;
        script.Sort();

    }
}
#endif