using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class UCExtensions
{
    public static Vector3 GetRandomInRange(this Vector3 pos, float rangeX, float rangeY, float rangeZ)
    {
        float randX = UnityEngine.Random.Range(pos.x - rangeX, pos.x + rangeX);
        float randY = UnityEngine.Random.Range(pos.y - rangeY, pos.y + rangeY);
        float randZ = UnityEngine.Random.Range(pos.y - rangeZ, pos.y + rangeZ);
        return new Vector3(randX, randY, randZ);
    }

    public static Vector3 GetRandomInRange(this Vector3 pos, float range)
    {
        float randX = UnityEngine.Random.Range(pos.x - range, pos.x + range);
        float randY = UnityEngine.Random.Range(pos.y - range, pos.y + range);
        float randZ = UnityEngine.Random.Range(pos.y - range, pos.y + range);
        return new Vector3(randX, randY, randZ);
    }

    public static Vector3 Round(this Vector3 vector3, int decimalPlaces = 2)
    {
        return new Vector3(
            (float)Math.Round(vector3.x, decimalPlaces),
           (float)Math.Round(vector3.y, decimalPlaces),
           (float)Math.Round(vector3.z, decimalPlaces));
    }
    public static Vector3 ToVector3(this string sVector)
    {
        Vector3 result = new Vector3();
        try
        {
            if (sVector.StartsWith("(") && sVector.EndsWith(")"))
            {
                sVector = sVector.Substring(1, sVector.Length - 2);
            }

            // split the items
            string[] sArray = sVector.Split(',');

            // store as a Vector3
            result = new Vector3(
                float.Parse(sArray[0]),
                float.Parse(sArray[1]),
                float.Parse(sArray[2]));
        }
        catch
        {

        }
        return result;
    }

    public static string ProductNameSimplified
    {
        get
        {
            return Application.productName.ToLower().Replace(" ", "_");
        }
    }

    public static void SortChild(this Transform target, float distance, bool isHorizontal = true)
    {
        float startPos = ((target.childCount - 1) * distance) / 2f;
        if (isHorizontal)
        {
            startPos *= -1f;
        }
        for (int i = 0; i < target.childCount; i++)
        {
            var child = target.GetChild(i);
            var childPos = child.transform.localPosition;
            if (isHorizontal)
            {
                childPos.x = startPos;
                childPos.y = 0;
                startPos += distance;
            }
            else
            {
                childPos.y = startPos;
                childPos.x = 0;
                startPos -= distance;
            }
            child.transform.localPosition = childPos;
        }
    }

    public static void SetAlpha(this Image image, float alpha)
    {
        var color = image.color;
        color.a = alpha;
        image.color = color;
    }
    public static void SetAlpha(this SpriteRenderer image, float alpha)
    {
        var color = image.color;
        color.a = alpha;
        image.color = color;
    }
    public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
    {
        if (rectTransform == null) return;
        Vector2 size = rectTransform.rect.size;
        Vector2 deltaPivot = rectTransform.pivot - pivot;
        Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);
        rectTransform.pivot = pivot;
        rectTransform.localPosition -= deltaPosition;
    }

}
