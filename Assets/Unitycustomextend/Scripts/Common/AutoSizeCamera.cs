using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSizeCamera : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] Vector2 defaultScreen = new Vector2(1920, 1080);
    // Start is called before the first frame update
    void Start()
    {
        float DefaultRatio = defaultScreen.y / defaultScreen.x;
        float currentRatio = Screen.height * 1f / Screen.width ;
        float ratio = currentRatio / DefaultRatio;
        mainCamera.orthographicSize *= ratio;
        mainCamera.fieldOfView *= ratio;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
