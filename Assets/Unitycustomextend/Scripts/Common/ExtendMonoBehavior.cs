using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendMonoBehavior : MonoBehaviour
{
    public bool showDebug;
    // Start is called before the first frame update
    public virtual void Start()
    {
        StartCoroutine(IEOneFrameAfterStart());
    }

    IEnumerator IEOneFrameAfterStart()
    {
        yield return new WaitForEndOfFrame();

        OneFrameAfterStart();
    }

    // Called one frame after Start
    public virtual void OneFrameAfterStart()
    {

    }

    public void Log(string message)
    {
        if(showDebug)
        {
            Debug.Log(message);
        }
    }
}
