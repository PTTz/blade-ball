using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class ExportTexture : MonoBehaviour
{
    [SerializeField] string dirPath = "D:/SaveImages";
    [SerializeField] string fileName = "SkinAvatar";
    [SerializeField] string fileExtension = "png";
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SaveTexture()
    {
        Texture texture = GetComponent<Camera>().targetTexture;
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = new RenderTexture(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);
        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();

        Color[] pixels = texture2D.GetPixels();

        RenderTexture.active = currentRT;

        byte[] bytes = texture2D.EncodeToPNG();
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }
        string path = $"{dirPath}/{fileName}.{fileExtension}";
        File.WriteAllBytes(path, bytes);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(ExportTexture))]
public class Car_Inspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var script = (ExportTexture)target;
        if (GUILayout.Button("Save"))
        {
            script.SaveTexture();
        }

    }
}
#endif