using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OdinExample : MonoBehaviour
{
    //============================================================= STATS CHANGE IN PLAY MODE

    [Title("STATS CHANGE IN PLAY MODE", titleAlignment: TitleAlignments.Centered)]

    [DisableInEditorMode]
    [SerializeField] private bool _value1;

    [DisableInEditorMode] public bool value2;

    //============================================================= STATS CHANGE IN EDITOR MODE

    [Title("STATS CHANGE IN EDITOR MODE", titleAlignment: TitleAlignments.Centered)]

    [GUIColor(0, 1, 1, 1)]
    [SerializeField] private bool _color0111;

    [GUIColor(0, 0, 1, 1)]
    [SerializeField] private bool _color0011;

    [GUIColor(0, 0, 0, 1)]
    [SerializeField] private bool _color0001;

    [GUIColor(1, 0, 0, 1)]
    [SerializeField] private bool _color1001;

    [GUIColor(1, 1, 0, 1)]
    [SerializeField] private bool _color1101;

    [GUIColor(1, 0, 1, 1)]
    [SerializeField] private bool _color1011;

    [GUIColor(1, 1, 1, 1)]
    [SerializeField] private bool _color1111;

    [GUIColor(0, 1, 0, 1)]
    [SerializeField] private bool _color0101;

    [GUIColor(0.75f, 1, 1, 1)]
    [SerializeField] private bool _color75_1_1_1;

    [GUIColor(0.75f, 1, 0, 1)]
    [SerializeField] private bool _color75_1_0_1;

    [GUIColor(0.75f, 0.75f, 0, 1)]
    [SerializeField] private bool _color75_75_0_1;

    [PropertyRange(0, 100)]
    public float range;

    //============================================================= REFERENT

    [Title("REFERENT", titleAlignment: TitleAlignments.Centered)]

    [SerializeField] private bool _value5;

    [FoldoutGroup("Group1")] public bool element1 = true;
    [FoldoutGroup("Group1")] public int element2 = 1;

    [BoxGroup("Male", centerLabel: true)] public Vector3 equiptPosMale;
    [BoxGroup("Male")] public Vector3 equiptRotaMale;

}
