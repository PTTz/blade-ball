using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCustomExtension.Generic
{
    public class PoolObjects : Singleton<PoolObjects>
    {
        Dictionary<string, RecylableObject> prefabPool = new Dictionary<string, RecylableObject>();

        Dictionary<string, Queue<RecylableObject>> instantiatedObjects = new Dictionary<string, Queue<RecylableObject>>();

        [SerializeField] List<PrepareObj> PreloadObjs;

        private void Start()
        {
            // khoi tao list cac obj reload ra luu vao
            List<RecylableObject> objs = new List<RecylableObject>();

            // spawn truoc cac obj
            foreach (var obj in PreloadObjs)
            {
                for (int i = 0; i < obj.prepareNumber; i++)
                {
                    RecylableObject newObj = SpawnObject(obj.obj);
                    objs.Add(newObj);
                }
            }

            // sau khi spawn -> recycle all
            foreach (var obj in objs)
            {
                obj.RecyclePreload();
            }

            // neu vua spawn vua recyle ma ko luu vao list, so luong preload obj se ko du vi obj recycle chinh no
        }

        bool HasPrefabInPool(string name)
        {
            return prefabPool.ContainsKey(name);
        }

        void AddToPool(RecylableObject newObject)
        {
            prefabPool.Add(newObject.name, newObject);
        }

        public RecylableObject SpawnObject(RecylableObject prefab, Transform parent = null)
        {
            if (!HasPrefabInPool(prefab.name))
            {
                AddToPool(prefab);
            }
            var obj = GetNewObject(prefab.name);
            if (obj)
            {
                if (parent) obj.transform.SetParent(parent);
                obj.gameObject.SetActive(true);
                obj.transform.localPosition = Vector3.zero;
                obj.OnSpawn();
            }
            return obj;
        }

        public RecylableObject SpawnObject(RecylableObject prefab, Vector3 position)
        {
            if (!HasPrefabInPool(prefab.name))
            {
                AddToPool(prefab);
            }
            var obj = GetNewObject(prefab.name);
            if (obj)
            {
                obj.gameObject.SetActive(true);
                obj.transform.position = position;
                obj.OnSpawn();
            }
            return obj;
        }

        public RecylableObject SpawnObject(RecylableObject prefab, Vector3 position, Quaternion rotation)
        {
            if (!HasPrefabInPool(prefab.name))
            {
                AddToPool(prefab);
            }
            var obj = GetNewObject(prefab.name);
            if (obj)
            {
                obj.gameObject.SetActive(true);
                obj.transform.position = position;
                obj.transform.rotation = rotation;
                obj.OnSpawn();
            }
            return obj;
        }

        public RecylableObject SpawnObject(RecylableObject prefab, Vector3 position, Vector3 angle)
        {
            if (!HasPrefabInPool(prefab.name))
            {
                AddToPool(prefab);
            }
            var obj = GetNewObject(prefab.name);
            if (obj)
            {
                obj.gameObject.SetActive(true);
                obj.transform.position = position;
                obj.transform.localEulerAngles = angle;
                obj.OnSpawn();
            }
            return obj;
        }

        public RecylableObject SpawnObject(RecylableObject prefab, Vector3 position, Vector3 angle, Vector3 scale)
        {
            if (!HasPrefabInPool(prefab.name))
            {
                AddToPool(prefab);
            }
            var obj = GetNewObject(prefab.name);
            if (obj)
            {
                obj.gameObject.SetActive(true);
                obj.transform.position = position;
                obj.transform.localEulerAngles = angle;
                obj.transform.localScale = scale;
                obj.OnSpawn();
            }
            return obj;
        }

        public T SpawnObject<T>(RecylableObject prefab, Transform parent = null)
        {
            RecylableObject newObj = SpawnObject(prefab, parent);
            return newObj.GetComponent<T>();
        }

        public T SpawnObject<T>(RecylableObject prefab, Vector3 position, Transform parent = null)
        {
            RecylableObject newObj = SpawnObject(prefab, position);
            return newObj.GetComponent<T>();
        }

        public T SpawnObjectAs<T>(RecylableObject prefab, Vector3 position, Vector3 angle)
        {
            RecylableObject newObj = SpawnObject(prefab, position, angle);
            return newObj.GetComponent<T>();
        }

        public T SpawnObjectAs<T>(RecylableObject prefab, Vector3 position, Quaternion rotation)
        {
            RecylableObject newObj = SpawnObject(prefab, position, rotation);
            return newObj.GetComponent<T>();
        }

        RecylableObject GetNewObject(string prefabName)
        {
            if (instantiatedObjects.ContainsKey(prefabName))
            {
                if (instantiatedObjects[prefabName].Count > 0)
                {
                    var obj = instantiatedObjects[prefabName].Dequeue();
                    return obj;
                }
            }
            var prefab = GetPrefab(prefabName);
            if (prefab)
            {
                var obj = Instantiate(prefab);
                obj.SetOriginalName(prefabName);
                return obj;
            }
            return null;
        }

        public void DestroyObject(RecylableObject obj)
        {
            var objOriginalName = obj.OriginalName;

            if (!instantiatedObjects.ContainsKey(objOriginalName))
            {
                instantiatedObjects[objOriginalName] = new Queue<RecylableObject>();
            }

            instantiatedObjects[objOriginalName].Enqueue(obj);

            obj.gameObject.SetActive(false);

            obj.transform.SetParent(transform);
        }

        RecylableObject GetPrefab(string prefabName)
        {
            if (prefabPool.ContainsKey(prefabName))
            {
                return prefabPool[prefabName];
            }
            else
            {
                return null;
            }
        }

    }

    [Serializable]

    public class PrepareObj
    {
        public RecylableObject obj;
        public int prepareNumber = 1;
    }
}