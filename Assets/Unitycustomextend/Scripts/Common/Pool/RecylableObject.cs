using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UnityCustomExtension.Generic
{
    [DisallowMultipleComponent]
    public class RecylableObject : MonoBehaviour
    {
        [Title("Recylable object", titleAlignment: TitleAlignments.Centered)]

        [SerializeField] bool IsNotSpawnObj = true;

        [SerializeField] bool isAutoRecyle;

        [ShowIf("@isAutoRecyle == true")] 
        public float timeAutoRecyle;

        [SerializeField] bool SoundOnDeath;

        [ShowIf("@SoundOnDeath == true")]
        public RecylableObject SoundOnDeathObj;

        public Action OnRecylable;

        string originalName;

        bool isAvailabe;
        public virtual void Start()
        {
            IsAvailable = true;

            if(IsNotSpawnObj && isAutoRecyle)
            {
                StartCoroutine(IEAutoRecyle());
            }
        }

        protected bool IsAvailable
        {
            get => isAvailabe;
            private set
            {
                isAvailabe = value;
            }
        }
        public string OriginalName => originalName;

        public virtual void OnSpawn()
        {
            IsAvailable = true;

            transform.SetParent(null);

            if (isAutoRecyle)
            {
                StartCoroutine(IEAutoRecyle());
            }

            IsNotSpawnObj = false;
        }

        public void OnSpawnBase()
        {
            IsAvailable = true;

            transform.SetParent(null);

            if (isAutoRecyle)
            {
                StartCoroutine(IEAutoRecyle());
            }
        }

        public void SetOriginalName(string originalName)
        {
            this.originalName = originalName;
        }
        IEnumerator IEAutoRecyle()
        {
            yield return new WaitForSecondsRealtime(timeAutoRecyle);
            Recycle();
        }
        public virtual void Recycle()
        {
            OnRecylable?.Invoke();
            if (IsNotSpawnObj)
            {
                Destroy(gameObject);
            }
            else
            {
                if (!IsAvailable) return;
                IsAvailable = false;
                PoolObjects.Ins.DestroyObject(this);
            }

            if (SoundOnDeathObj != null)
            {
                PoolObjects.Ins.SpawnObject(SoundOnDeathObj);
            }
        }
        public void RecyclePreload()
        {
            if (IsNotSpawnObj)
            {
                Destroy(gameObject);
            }
            else
            {
                if (!IsAvailable) return;
                IsAvailable = false;
                PoolObjects.Ins.DestroyObject(this);
            }

            if (SoundOnDeathObj != null)
            {
                PoolObjects.Ins.SpawnObject(SoundOnDeathObj);
            }
        }
    }
}