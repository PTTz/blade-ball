using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DateTimeExtension
{
    public static bool IsDayAfter(this DateTime current, DateTime compare)
    {
        if (current.Year > compare.Year)
        {
            return true;
        }
        else if (current.Year == compare.Year)
        {
            if (current.Month > compare.Month) return true;
            else if (current.Month == compare.Month)
            {
                return current.Day > compare.Day;
            }
        }
        return false;
    }
}
