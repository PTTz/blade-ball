﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : SerializedMonoBehaviour where T : Singleton<T>
{
    private static T instance;
    public static T Ins { get => instance; }

    [SerializeField] private bool needDontDestroy = false;

    public bool showDebug;

    public virtual void Awake()
    {
        if (instance == null)
        {
            instance = (T)this;
            if (needDontDestroy) DontDestroyOnLoad(gameObject);
            SetUp();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public virtual void Start()
    {
        StartCoroutine(IEOneFrameAfterStart());
    }

    IEnumerator IEOneFrameAfterStart()
    {
        yield return new WaitForEndOfFrame();

        OneFrameAfterStart();
    }

    // Called one frame after Start
    public virtual void OneFrameAfterStart()
    {

    }

    public void Log(string message)
    {
        if (showDebug)
        {
            Debug.Log(message);
        }
    }

    private void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
    protected virtual void SetUp()
    {

    }

}