using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class TakeScreenShotExt : MonoBehaviour
{
    [MenuItem("UnityCustomExtension/ TakeScreenShot")]
    public static void TakeScreenShotMenu()
    {
        string path = $"D:/Screenshots {Application.productName}/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        string fileName = path + "/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png";
        Debug.Log(fileName);
        ScreenCapture.CaptureScreenshot(fileName);
        UnityEditor.AssetDatabase.Refresh();
    }
}
