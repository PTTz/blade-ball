using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ShowFPS : MonoBehaviour
{
    int FPS;
    [SerializeField] TextMeshProUGUI FPSText;

    void Update()
    {
        float current = 0;
        current = Time.frameCount / Time.time;
        FPS = (int)current;
        if(FPS < 30)
        {
            FPSText.text = "<color=red>"+ FPS.ToString() + " FPS </color>";
        }
        else if( FPS < 45)
        {
            FPSText.text = "<color=yellow>" + FPS.ToString() + " FPS </color>";
        }
        else
        {
            FPSText.text = "<color=green>" + FPS.ToString() + " FPS </color>";
        }
    }
}
