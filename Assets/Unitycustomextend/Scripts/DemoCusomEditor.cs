using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DemoCusomEditor : MonoBehaviour
{
    [Header("Header 1")]
    public DisplayType displayType;
    public int Int_;
    #region :: Region 1 ::
    public float Float_;
    public bool Bool_;
    #endregion
    public string String_;
    [Header("Header 2")]
    public GameObject Obj_;
    public Transform Tranform_;
    #region :: Region 2 ::
    public List<int> ListInt_;
    public List<GameObject> ListObjs_;
    #endregion

    public void DemoCustomEditorButton()
    {
        // do something here
        Debug.Log("Click demo button con custom editor");
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(DemoCusomEditor))]
public class CustomEditorDemo : Editor
{
    SerializedProperty displayType;
    SerializedProperty Int_;
    SerializedProperty Float_;
    SerializedProperty Bool_;
    SerializedProperty String_;
    SerializedProperty Obj_;
    SerializedProperty Tranform_;
    SerializedProperty ListInt_;
    SerializedProperty ListObjs_;

    private void OnEnable()
    {
        // Find referent
        displayType = serializedObject.FindProperty("displayType");
        Int_ = serializedObject.FindProperty("Int_");
        Float_ = serializedObject.FindProperty("Float_");
        Bool_ = serializedObject.FindProperty("Bool_");
        String_ = serializedObject.FindProperty("String_");
        Obj_ = serializedObject.FindProperty("Obj_");
        Tranform_ = serializedObject.FindProperty("Tranform_");
        ListInt_ = serializedObject.FindProperty("ListInt_");
        ListObjs_ = serializedObject.FindProperty("ListObjs_");
    }
    public override void OnInspectorGUI()
    {
        // Show - Hide element
        serializedObject.Update();
        DemoCusomEditor myBehavior = (DemoCusomEditor)target;
        if (myBehavior != null)
        {
            switch (myBehavior.displayType)
            {
                case DisplayType.ShowAll:
                    ShowAllField();

                    break;
                case DisplayType.HideAll:
                    HideAllField();
                    break;
                case DisplayType.HideSomething:
                    HideSomething();
                    break;
                default:
                    break;
            }
        }
        // Apply value
        serializedObject.ApplyModifiedProperties();

        // Create Button
        if (GUILayout.Button("ButtonName"))
        {
            myBehavior.DemoCustomEditorButton();
            // save setup
            EditorUtility.SetDirty(myBehavior);
            AssetDatabase.SaveAssetIfDirty(myBehavior);
        }
    }

    void ShowAllField()
    {
        EditorGUILayout.PropertyField(displayType);
        // Write = show <=> dont write = hide
        EditorGUILayout.TextArea("--Area 1--", GUIStyle.none);
        Int_.intValue = EditorGUILayout.IntField(Int_.intValue);
        Float_.floatValue = EditorGUILayout.FloatField(Float_.floatValue);
        Bool_.boolValue = EditorGUILayout.Toggle(Bool_.boolValue);
        String_.stringValue = EditorGUILayout.TextField(String_.stringValue);
        EditorGUILayout.PropertyField(Obj_);
        EditorGUILayout.TextArea("-- Area 51 --",GUIStyle.none);
        EditorGUILayout.PropertyField(Tranform_);
        EditorGUILayout.PropertyField(ListInt_);
        EditorGUILayout.PropertyField(ListObjs_);
    }
    void HideAllField()
    {
        EditorGUILayout.PropertyField(displayType);
        // Show nothing , write no thing
    }
    void HideSomething()
    {
        SerializedProperty iterator = serializedObject.GetIterator();
        bool getChild = true;
        while (iterator.NextVisible(getChild))
        {
            getChild = false;
            if (iterator.propertyPath == Int_.propertyPath) // Hide Int_
            {
                continue;
            }
            if (iterator.propertyPath == Float_.propertyPath) // Hide Float_
            {
                continue;
            }
            EditorGUILayout.PropertyField(iterator, true);
        }
    }
    void HowToSetValue(DemoCusomEditor myBehavior)
    {
        // Demo how to set value of class mono field
        myBehavior.Int_ = Int_.intValue;
        myBehavior.Float_ = Float_.floatValue;
        myBehavior.Bool_ = Bool_.boolValue;
        myBehavior.String_ = String_.stringValue;
        myBehavior.Obj_ = Obj_.objectReferenceValue as GameObject;
        myBehavior.Tranform_ = Tranform_.objectReferenceValue as Transform;

        myBehavior.ListInt_ = new List<int>(ListInt_.arraySize);
        for (int i = 0; i < myBehavior.ListInt_.Count; i++)
        {
            myBehavior.ListInt_.Add(ListInt_.GetArrayElementAtIndex(i).intValue);
        }

        myBehavior.ListObjs_ = new List<GameObject>(ListInt_.arraySize);
        for (int i = 0; i < myBehavior.ListInt_.Count; i++)
        {
            myBehavior.ListObjs_.Add(ListObjs_.GetArrayElementAtIndex(i).objectReferenceValue as GameObject);
        }
    }
}
#endif
public enum DisplayType
{
    ShowAll, HideAll, HideSomething
}
