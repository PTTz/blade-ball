using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadingSpriteRenderer : MonoBehaviour
{
    [SerializeField] bool isAutoPlay;

    [SerializeField] bool isCollide;

    [SerializeField] SpriteRenderer mainSR;

    [SerializeField] float timeDo = 1f;

    [SerializeField] float timeDo2 = 1f;

    [SerializeField] float fadeFull = 1;

    [SerializeField] float fade = 0.5f;

    [SerializeField] int loop;

    // Start is called before the first frame update
    void Start()
    {
        if (isAutoPlay)
        {
            PlayEffect(loop);
        }
    }

    public void PlayEffect(int loop)
    {
        StopEffect();
        mainSR.SetAlpha(fade);
        Sequence seq = DOTween.Sequence();
        seq.Insert(0, mainSR.DOFade(fadeFull, timeDo).SetEase(Ease.InOutSine));
        seq.Insert(timeDo, mainSR.DOFade(fade, timeDo2).SetEase(Ease.InOutSine));
        seq.SetLoops(loop).SetId(GetInstanceID()).SetUpdate(true);
    }

    public void StopEffect()
    {
        DOTween.Kill(GetInstanceID());
        mainSR.SetAlpha(fadeFull);

    }

}
