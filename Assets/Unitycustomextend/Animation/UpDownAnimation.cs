using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownAnimation : MonoBehaviour
{
    [SerializeField] bool Autoplay = true;

    [SerializeField] float distance = 10;

    [SerializeField] float timeDo = 1f;

    Sequence seq;

    Vector3 startLocalPos;

    // Start is called before the first frame update
    private void Start()
    {
        seq = DOTween.Sequence();
        startLocalPos = transform.localPosition;
        if (Autoplay)
        {
            PlayAnim();
        }
    }

    public void PlayAnim()
    {
        Vector3 upPos = transform.localPosition;
        upPos.y += distance;
        Vector3 downPos = transform.localPosition;
        downPos.y -= distance;
        transform.localPosition = upPos;
        seq.Insert(0, transform.DOLocalMove(downPos, timeDo).SetEase(Ease.InOutSine));
        seq.Insert(timeDo, transform.DOLocalMove(upPos, timeDo).SetEase(Ease.InOutSine));
        seq.SetLoops(-1).SetUpdate(true);
    }
    public void StopAnim()
    {
        DOTween.Kill(seq);
    }
    public void StopAnimAndResetPosition()
    {
        DOTween.Kill(seq);
        transform.localPosition = startLocalPos;
    }
}
