using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class FreeRoll : MonoBehaviour
{
    public float x_Power;
    public float y_Power;
    public float z_Power;
    float x = 0;
    float y = 0;
    float z = 0;
    bool UpdateRoll = true;

    private void FixedUpdate()
    {
        if (!UpdateRoll) return;
        Quaternion rota = transform.localRotation;
        x += Time.deltaTime * x_Power;
        y += Time.deltaTime * y_Power;
        z += Time.deltaTime * z_Power;
        transform.localRotation = Quaternion.Euler(x, y, z);
    }

    public void SetNeedRoll(bool canRoll)
    {
        UpdateRoll = canRoll;
    }
}
