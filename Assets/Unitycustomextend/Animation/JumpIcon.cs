using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityCustomExtension.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpIcon : RecylableObject
{
    [SerializeField] Image mainImage;

    [SerializeField] float range = 50;

    [SerializeField] float forceJump = 25;

    [SerializeField] float timeJump = 0.3f;

    [SerializeField] float timeFade = 0.5f;

    public override void OnSpawn()
    {
        base.OnSpawn();
        mainImage.SetAlpha(1);
        Vector3 randPos = transform.position.GetRandomInRange(range, range, 0);
        transform.DOJump(randPos, forceJump, 1, timeJump).OnComplete(() =>
        {
            mainImage.DOFade(0, timeFade).OnComplete(() =>
            {
                Recycle();
            }).SetUpdate(true);
        }).SetUpdate(true);

    }
}
