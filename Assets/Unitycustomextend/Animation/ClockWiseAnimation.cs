using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCustomExtension.Animation
{
    public class ClockWiseAnimation : MonoBehaviour
    {
        [SerializeField] float timeDo = 0.5f;
        RectTransform rect;
        Sequence currentSeq;
        private void Awake()
        {
            rect = GetComponent<RectTransform>();
        }
        private void OnEnable()
        {
            StopClock();
            StartClock();
        }

        void StartClock()
        {
            rect.localEulerAngles = new Vector3(0, 0, 89f);
            currentSeq = DOTween.Sequence();
            currentSeq.Insert(0, rect.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, -89), timeDo).SetEase(Ease.InOutSine));
            currentSeq.Insert(timeDo, rect.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, 89), timeDo).SetEase(Ease.InOutSine));
            currentSeq.SetLoops(-1).SetUpdate(true);
        }

        public void StopClock()
        {
            currentSeq.Kill();
        }

        public int GetMultiplyByAngle()
        {
            float angle = rect.localRotation.z;
            if (angle <= -54f / 90f) return 5;
            if (angle < -19f / 90f && angle >= -55f / 90f) return 4;
            if (angle < 40f / 90f && angle >= -18f / 90f) return 3;
            return 2;
        }
    }
}