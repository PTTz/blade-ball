using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;

namespace UnityCustomExtension.Animation
{
    public class ScaleAnim : MonoBehaviour
    {
        public bool autoPlay = true;
        public bool randomScaleTime;

        public float originalScale = 1;
        public float scaleTo = 1.3f;

        [Header("Fixed Time")]
        public float timeIn = 0.35f;
        public float timeOut = 0.2f;

        [Header("Random Time")]
        public float timeInMin = 0.1f;
        public float timeInMax = 0.7f;
        public float timeOutMin = 0.1f;
        public float timeOutMax = 0.4f;

        private void OnEnable()
        {
            if (autoPlay) PlayAnim();
        }

        public void PlayAnim()
        {
            if (!randomScaleTime)
            {
                Anim(timeIn, timeOut);
            }
            else
            {
                RandomTimeAnim();
            }
        }
        void Anim(float timeIn_, float timeOut_)
        {
            transform.localScale = Vector3.one * originalScale;
            Sequence seq = DOTween.Sequence();
            seq.Insert(0, transform.DOScale(Vector3.one * scaleTo, timeIn_).SetEase(Ease.InOutSine));
            seq.Insert(timeIn_, transform.DOScale(Vector3.one * originalScale, timeOut_).SetEase(Ease.InOutSine));
            seq.SetLoops(-1).SetUpdate(true).SetId($"ScaleAnim {GetInstanceID()}");
            //Debug.Log("SCALE ANIM played");

        }
        void RandomTimeAnim()
        {
            float timeIn = Random.Range(timeInMin, timeInMax);
            float timeOut = Random.Range(timeOutMin, timeOutMax);
            Anim(timeIn, timeOut);
        }
        public void StopAnim()
        {
            DOTween.Kill($"ScaleAnim {GetInstanceID()}");
            transform.localScale = Vector3.one * originalScale;
            //Debug.Log("SCALE ANIM stoped");
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(ScaleAnim))]
    public class CustomEditorScaleAnim : Editor
    {
        SerializedProperty autoPlay_;
        SerializedProperty originalScale_;
        SerializedProperty scaleTo_;
        SerializedProperty timeIn_;
        SerializedProperty timeOut_;

        SerializedProperty randomScaleTime_;
        SerializedProperty timeInMin_;
        SerializedProperty timeInMax_;
        SerializedProperty timeOutMin_;
        SerializedProperty timeOutMax_;

        private void OnEnable()
        {
            autoPlay_ = serializedObject.FindProperty("autoPlay");
            originalScale_ = serializedObject.FindProperty("originalScale");
            scaleTo_ = serializedObject.FindProperty("scaleTo");
            timeIn_ = serializedObject.FindProperty("timeIn");
            timeOut_ = serializedObject.FindProperty("timeOut");

            randomScaleTime_ = serializedObject.FindProperty("randomScaleTime");
            timeInMin_ = serializedObject.FindProperty("timeInMin");
            timeInMax_ = serializedObject.FindProperty("timeInMax");
            timeOutMin_ = serializedObject.FindProperty("timeOutMin");
            timeOutMax_ = serializedObject.FindProperty("timeOutMax");
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            ScaleAnim myScaleAnim = (ScaleAnim)target;
            if (myScaleAnim != null)
            {
                if (myScaleAnim.randomScaleTime)
                {
                    EditorGUILayout.PropertyField(autoPlay_);
                    EditorGUILayout.PropertyField(randomScaleTime_);

                    EditorGUILayout.PropertyField(originalScale_);
                    EditorGUILayout.PropertyField(scaleTo_);

                    EditorGUILayout.PropertyField(timeInMin_);
                    EditorGUILayout.PropertyField(timeInMax_);
                    EditorGUILayout.PropertyField(timeOutMin_);
                    EditorGUILayout.PropertyField(timeOutMax_);
                }
                else
                {
                    EditorGUILayout.PropertyField(autoPlay_);
                    EditorGUILayout.PropertyField(randomScaleTime_);

                    EditorGUILayout.PropertyField(originalScale_);
                    EditorGUILayout.PropertyField(scaleTo_);
                    EditorGUILayout.PropertyField(timeIn_);
                    EditorGUILayout.PropertyField(timeOut_);

                }
            }
            // Apply value
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}
