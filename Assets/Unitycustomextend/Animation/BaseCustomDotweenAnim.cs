using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCustomExtension.Animation
{
    public class BaseCustomDotweenAnim : MonoBehaviour
    {
        public bool AutoPlayOnEnable;
        public Sequence seq;
        [HideInInspector] public string DoTweenID;

        private void OnEnable()
        {
            if (AutoPlayOnEnable)
            {
                PlayAnim();
            }
        }
        private void Start()
        {
            seq = DOTween.Sequence();
            DoTweenID = $"CustomDotweenAnim{GetInstanceID()}";
        }

        public virtual void PlayAnim()
        {

        }
        public virtual void StopAnim() 
        {
            DOTween.Kill(seq);
            DOTween.Kill(DoTweenID);
        }
    }
}

