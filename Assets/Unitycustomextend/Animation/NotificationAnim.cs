using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationAnim : MonoBehaviour
{
    [SerializeField] bool autoPlay;


    [SerializeField] Vector3 stretchScale;

    [SerializeField] Vector3 stretchScale2;

    [SerializeField] Vector3 normalScale = Vector3.one;

    [SerializeField] Vector3 squashUp;

    RectTransform rect;

    [SerializeField] float TimeMove = 0.5f;

    [SerializeField] float TimeDelay = 0.5f;

    [SerializeField] float MoveDistance = 10;

    [SerializeField] Vector3 TargetScale = Vector3.one;

    [SerializeField] Vector3 OriginalScale = new Vector3(2, 0.5f, 1);

    [SerializeField] Ease MoveEase = Ease.OutBack;

    [SerializeField] Ease ScaleEase = Ease.OutQuad;

    Vector3 originalPos;

    Sequence sequence;
    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        originalPos = rect.localPosition;
        if (autoPlay)
        {
            PlayEffect();
        }
    }
    public void PlayEffect()
    {
        StopEffect();
        rect.localScale = OriginalScale;
        Vector3 targetPos = originalPos;
        targetPos.y += MoveDistance;
        sequence = DOTween.Sequence();
        sequence.Insert(0, rect.DOLocalMove(targetPos, TimeMove).SetEase(MoveEase));
        sequence.Insert(0, rect.DOScale(TargetScale, TimeMove).SetEase(ScaleEase));
        sequence.Insert(TimeMove , rect.DOLocalMove(originalPos, TimeMove).SetEase(MoveEase));
        sequence.Insert(TimeMove , rect.DOScale(OriginalScale, TimeMove).SetEase(ScaleEase));
        sequence.SetLoops(-1).SetUpdate(true);

    }

    public void StopEffect()
    {
        sequence?.Kill();
        rect.localScale = TargetScale;
        rect.localPosition = originalPos;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

}
