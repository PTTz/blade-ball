using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAndShakeAnim : MonoBehaviour
{
    [SerializeField] bool autoPlay;

    [SerializeField] Vector3 stretchScale = new Vector3(2f, 0.5f, 1f);

    [SerializeField] Vector3 stretchScale2 = new Vector3(1.428571f, 0.7f, 1f);

    [SerializeField] Vector2 step1Pivot = new Vector2(0.5f, 0.2f);

    [SerializeField] Vector2 step2Pivot = new Vector2(0.5f, 0.5f);

    [SerializeField] float moveDistance = 15f;

    [SerializeField] float shakeAngle = 15f;

    Vector3 normalScale = Vector3.one;

    float timeStretch = 0.2f;

    float timeStretch2 = 0.1f;

    float timeDelay = 2f;

    float timeMove = 0.2f;

    float timeMove2 = 0.1f;

    float timeShake = 0.1f;

    Ease moveUpEase = Ease.Unset;

    Ease moveDownEase = Ease.Unset;

    RectTransform rect;

    Vector3 originalPos;

    Sequence sequence;

    IEnumerator currentEffect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        originalPos = rect.localPosition;
    }
    private void OnEnable()
    {
        if (autoPlay)
        {
            PlayEffect();
        }
    }
    public void PlayEffect()
    {
        StopEffect();
        currentEffect = IEDoEffect();
        StartCoroutine(currentEffect);
    }
    IEnumerator IEDoEffect()
    {
        int numShake = 3;
        while (true)
        {
            rect.SetPivot(step1Pivot);
            rect.DOScale(stretchScale, timeStretch).SetUpdate(true).SetId(GetInstanceID());
            yield return new WaitForSecondsRealtime(timeStretch);
            rect.DOScale(normalScale, timeMove).SetUpdate(true);
            rect.DOLocalMoveY(originalPos.y + moveDistance, timeMove).SetUpdate(true).SetId(GetInstanceID()).SetEase(moveUpEase);
            yield return new WaitForSecondsRealtime(timeMove);
            rect.SetPivot(step2Pivot);
            rect.DORotate(new Vector3(0, 0, -shakeAngle), timeShake / 2).SetUpdate(true).SetId(GetInstanceID()).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(timeShake / 2);
            for (int i = 0; i < numShake; i++)
            {
                rect.DORotate(new Vector3(0, 0, shakeAngle), timeShake).SetUpdate(true).SetId(GetInstanceID()).SetEase(Ease.Linear);
                yield return new WaitForSecondsRealtime(timeShake);
                rect.DORotate(new Vector3(0, 0, -shakeAngle), timeShake).SetUpdate(true).SetId(GetInstanceID()).SetEase(Ease.Linear);
                yield return new WaitForSecondsRealtime(timeShake);
            }
            rect.DORotate(new Vector3(0, 0, 0), timeShake / 2).SetUpdate(true).SetId(GetInstanceID()).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(timeShake / 2);
            rect.SetPivot(step2Pivot);
            rect.DOLocalMoveY(originalPos.y, timeMove2).SetUpdate(true).SetId(GetInstanceID()).SetEase(moveDownEase);
            yield return new WaitForSecondsRealtime(timeMove2);
            rect.DOScale(stretchScale2, timeStretch2).SetUpdate(true).SetId(GetInstanceID());
            yield return new WaitForSecondsRealtime(timeStretch2);
            rect.DOScale(normalScale, timeStretch2).SetUpdate(true).SetId(GetInstanceID());
            yield return new WaitForSecondsRealtime(timeStretch2);
            yield return new WaitForSecondsRealtime(timeDelay);
        }
    }
    public void StopEffect()
    {
        if (currentEffect != null)
        {
            StopCoroutine(currentEffect);
        }
        DOTween.Kill(GetInstanceID());
        rect.localScale = normalScale;
        rect.localPosition = originalPos;
    }
}