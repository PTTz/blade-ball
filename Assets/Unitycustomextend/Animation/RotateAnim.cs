using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCustomExtension.Animation
{
    public class RotateAnim : MonoBehaviour
    {
        public Vector3 targetRotate = new Vector3(0, 0, 360);

        public float time = 5;

        // Start is called before the first frame update
        void Start()
        {
            transform.DORotate(targetRotate, time, RotateMode.WorldAxisAdd).SetEase(Ease.Linear).SetLoops(-1).SetUpdate(true);
        }
    }
}