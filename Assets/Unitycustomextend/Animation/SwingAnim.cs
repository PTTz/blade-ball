using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityCustomExtension.Animation
{
    public class SwingAnim : BaseCustomDotweenAnim
    {
        [SerializeField] AnimationCurve curveIn;
        [SerializeField] AnimationCurve curveOut;
        [SerializeField] float TimeDo;
        [SerializeField] Vector3 AngleIn;
        [SerializeField] Vector3 AngleOut;

        public override void PlayAnim()
        {
            seq.Insert(0, transform.DOLocalRotate(AngleIn, TimeDo).SetEase(curveIn));
            seq.Insert(TimeDo, transform.DOLocalRotate(AngleOut, TimeDo).SetEase(curveOut));
            seq.SetLoops(-1).SetUpdate(true).SetId(DoTweenID);
        }
    }
}

