using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityCustomExtension.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogGUI : Singleton<DialogGUI>
{
    [SerializeField] bool IsDebug;
    [SerializeField] TextMeshProUGUI debugTMP;
    public string debugtext;
    [SerializeField] Button ClearBtn;

    private void Start()
    {
        ClearBtn.onClick.AddListener(Clear);
    }

    public void PrintDialogTMP(string text)
    {
        if (!IsDebug) return;

        StartCoroutine(IWaitForMainthread(text));
    }

    IEnumerator IWaitForMainthread(string text)// anti crash while update UI not on main thread
    {
        yield return new WaitForEndOfFrame();
        debugtext = debugtext + " \\\n  *********************  \\\n " + DateTime.Now + " - " + text;
        debugTMP.text = debugtext;
        Debug.Log(text);
    }

    void Clear()
    {
        debugtext = "";
        debugTMP.text = debugtext;
    }
}
