using MalbersAnimations;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : Singleton<LevelController>
{
    public Transform player;

    [DisableInEditorMode]
    public List<Transform> listPlayers;

    public List<Bot> listBots;

    public BallParent ballParent;

    public Ball ball;

    //----------------------------- Action ----------------------------

    // ball

    [HideInInspector]
    public Action OnBallReseted;

    [HideInInspector]
    public Action OnBallHited;

    [HideInInspector]
    public Action OnBallResetDone;

    [HideInInspector]
    public Action<Transform> OnBallHitPlayer;

    [HideInInspector]
    public Action<Transform> OnBallTraceNewTarget;

    // player

    [HideInInspector]
    public Action OnPlayerDie;

    [HideInInspector]
    public Action<int> OnAmountPlasyerLessChangeTo;

    [HideInInspector]
    public Action<bool> OnPlayerImmuteChangeTo;

    //-------------------------------------------------------------------

    public bool playerImmute = false;

    public bool isBallResetedDoneAndNotHitedYet;

    public Transform botClosestToBall;

    public Transform ballTarget;

    public override void Awake()
    {
        base.Awake();

        listPlayers.Clear();

        listPlayers.Add(player);

        foreach (var bot in listBots)
        {
            listPlayers.Add(bot.transform);
        }
    }

    public override void Start()
    {
        base.Start();

        Application.targetFrameRate = 60;

        OnBallResetDone += OnBallResetedFuntion;

        OnBallHited += OnBallHitedFuntion;

        OnBallTraceNewTarget+= OnBallTraceNewTargetFuntion;

        OnBallResetedFuntion();
    }


    void OnBallResetedFuntion()
    {
        isBallResetedDoneAndNotHitedYet = true;

        Bot choosenBot = listBots[0];

        foreach (var bot in listBots)
        {
            if (Vector3.Distance(ball.transform.position, bot.transform.position)
                < Vector3.Distance(ball.transform.position, choosenBot.transform.position))
            {
                choosenBot = bot;
            }
        }

        botClosestToBall = choosenBot.transform;

    }

    void OnBallHitedFuntion()
    {
        isBallResetedDoneAndNotHitedYet = false;
    }

    void OnBallTraceNewTargetFuntion(Transform target)
    {
        ballTarget = target;
    }

    public Transform GetTarget(Transform ball, Transform attacker)
    {
        if (listPlayers == null)
        {
            Debug.Log("ERROR"); return null;
        }

        if (listPlayers.Count == 1) return null;

        Transform target = listPlayers[0];

        if (attacker.GetInstanceID() == listPlayers[0].GetInstanceID())
        {
            target = listPlayers[1];
        }

        foreach (var tf in listPlayers)
        {
            if (Vector3.Distance(ball.position, tf.position)
                < Vector3.Distance(ball.position, target.position)
                && tf.GetInstanceID() != attacker.GetInstanceID())
            {
                target = tf;
            }
        }
        return target;
    }

    public void PlayerDie(Transform player)
    {

        if (player == this.player)
        {
            OnPlayerDie?.Invoke();
        }

        if (listPlayers.Contains(player))
        {
            listPlayers.Remove(player);

            OnAmountPlasyerLessChangeTo?.Invoke(listPlayers.Count);
        }
    }

    // Action register

    public void OnClickBtnChangePlayerImmute()
    {
        playerImmute = !playerImmute;

        OnPlayerImmuteChangeTo?.Invoke(playerImmute);
    }

    public void ResetBallSpeed()
    {
        OnBallReseted?.Invoke();

        ballParent.ResetSpeed();
    }
}
