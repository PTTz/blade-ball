using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    public CanvasGameplayUI canvasGameplayUI;

    public BBPlayerRefHolder player;

    public BladeBallMoveController bbMoveController;

    private void Start()
    {
        canvasGameplayUI.OnDragJoyStick += bbMoveController.OnJoyStickDrag;

        canvasGameplayUI.OnJoyStickBtnUp += bbMoveController.OnJoyStickUp;

        canvasGameplayUI.OnClickBtnAttack += bbMoveController.OnClickBtnBlock;
    }
}
