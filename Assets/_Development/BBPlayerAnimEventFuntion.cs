using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBPlayerAnimEventFuntion : MonoBehaviour, IPlayerAnimEventFuntion
{
    [SerializeField] private BBPlayerRefHolder bbPlayerRefHolder;

    private void Start()
    {
        bbPlayerRefHolder.animClipEvent.OnAttackStart += OnAttackStartFuntion;

        bbPlayerRefHolder.animClipEvent.OnAttackTriggerStart += OnAttackTriggerStartFuntion;

        bbPlayerRefHolder.animClipEvent.OnAttackTriggerEnd += OnAttackTriggerEndFuntion;

        bbPlayerRefHolder.animClipEvent.OnAttackEnd += OnAttackEndFuntion;
    }

    public void OnAttackStartFuntion()
    {

    }

    public void OnAttackTriggerStartFuntion()
    {
        bbPlayerRefHolder.sphereBlockTrigger.gameObject.SetActive(true);
    }

    public void OnAttackTriggerEndFuntion()
    {
        bbPlayerRefHolder.sphereBlockTrigger.gameObject.SetActive(false);
    }

    public void OnAttackEndFuntion()
    {
        bbPlayerRefHolder.animator.SetLayerWeight(1, 0);
    }
}
