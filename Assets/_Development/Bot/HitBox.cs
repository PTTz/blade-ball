using MalbersAnimations.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBox : MonoBehaviour
{
    [SerializeField] private bool _teleAfterDie = true;

    [SerializeField] private int HP = 3;

    [SerializeField] private bool canTakeDmg = true;

    [SerializeField] private BBPlayerRefHolder bbPlayerRefHolder;

    public void SetCanTakeDmg(bool value)
    {
        canTakeDmg = value;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!canTakeDmg) return;

        if (other.CompareTag("Ball"))
        {
            HP -= 1;

            LevelController.Ins.ResetBallSpeed();

            if (HP <= 0)
            {
                LevelController.Ins.PlayerDie(bbPlayerRefHolder.transform);

                bbPlayerRefHolder.transform.gameObject.SetActive(false);

                if (_teleAfterDie)
                {
                    bbPlayerRefHolder.transform.transform.position = new Vector3(999, 999, 999);
                }
            }
        }
    }
}
