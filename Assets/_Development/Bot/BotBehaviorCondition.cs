using BehaviorDesigner.Runtime.Tasks;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBehaviorCondition : Conditional
{
    [HideInInspector]
    public Bot bot;

    public override void OnStart()
    {
        base.OnStart();

        bot = GetComponent<Bot>();
    }
}

public class BotConditionIsBallReseted : BotBehaviorCondition
{
    public override TaskStatus OnUpdate()
    {
        bool condition = LevelController.Ins.isBallResetedDoneAndNotHitedYet;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}

public class BotConditionIsPlayerNearestWithBall : BotBehaviorCondition
{
    public override TaskStatus OnUpdate()
    {
        bool condition = bot.transform == LevelController.Ins.botClosestToBall;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}

public class BotConditionNearBallEnoughToHit : BotBehaviorCondition
{
    public override TaskStatus OnUpdate()
    {
        bool condition = Vector3.Distance(bot.bbPlayerRefHolder.ball.transform.position,bot.transform.position)
                                        <= bot._nearEnoughToHit;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}

public class BotConditionTargetedByBall : BotBehaviorCondition
{
    public override TaskStatus OnUpdate()
    {
        bool condition = bot.transform == LevelController.Ins.ballTarget;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}

public class BotConditionTooNearBall : BotBehaviorCondition
{
    public override TaskStatus OnUpdate()
    {
        bool condition = Vector3.Distance(bot.bbPlayerRefHolder.ball.transform.position, bot.transform.position)
                                        <= 1;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}

public class BotConditionReactionWithBall : BotBehaviorCondition
{
    public ReactionWithBall reacionWithBall;

    public override TaskStatus OnUpdate()
    {
        bool condition = bot.reactionWithBall == reacionWithBall;

        return condition ? TaskStatus.Success : TaskStatus.Failure;
    }
}


