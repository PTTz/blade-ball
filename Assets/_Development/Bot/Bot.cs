using DG.Tweening;
using MalbersAnimations.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : MonoBehaviour
{
    public float _patrolDistance;

    public float _nearEnoughToHit;

    public float _currentNearEnoughToHit;

    public float cd = 2;

    bool canAttack = true;

    bool patrolling;

    public bool isPickedReactionWithBall;

    public ReactionWithBall reactionWithBall;

    public BBPlayerRefHolder bbPlayerRefHolder;

    private void Start()
    {
        //InvokeRepeating(nameof(Patrol), 0, 2);

        LevelController.Ins.OnBallTraceNewTarget += OnBallTraceNewTarget;

        _currentNearEnoughToHit = _nearEnoughToHit;

        //LevelController.Ins.OnBallReseted += OnBallReseted;
    }

    private void FixedUpdate()
    {
        //if (Vector3.Distance(bbPlayerRefHolder.ball.transform.position, transform.position) <= _currentNearEnoughToHit
        //    && canAttack)
        //{
        //    Attack();
        //}

        bbPlayerRefHolder.animator.SetFloat("InputMagnitude", bbPlayerRefHolder.navMeshAgent.velocity.magnitude);
    }

    public void MoveToHitBall()
    {
        // move to ball to hit ball

        //CancelInvoke(nameof(Patrol));

        patrolling = false;

        if (bbPlayerRefHolder.navMeshAgent.isOnNavMesh)
        {
            bbPlayerRefHolder.navMeshAgent.SetDestination(bbPlayerRefHolder.ball.transform.position);
        }

        _currentNearEnoughToHit = 2.5f;
    }

    void OnBallTraceNewTarget(Transform target)
    {
        //if(target == transform)
        //{
        //    targetin
        //}
    }

    public void Patrol()
    {
        if (patrolling) return;

        patrolling = true;

        bbPlayerRefHolder.navMeshAgent.isStopped = false;

        DOVirtual.Float(0, 1, 2, (x) => { }).OnComplete(() =>
        {
            patrolling = false;
        });

        _currentNearEnoughToHit = _nearEnoughToHit;

        if (bbPlayerRefHolder.navMeshAgent.isOnNavMesh)
        {
            bbPlayerRefHolder.navMeshAgent.SetDestination(GetRandomPatrolPosAround());
        }
    }

    public Vector3 GetRandomPatrolPosAround()
    {
        Vector3 here = transform.position;

        float x = Random.Range(-_patrolDistance, _patrolDistance);

        float z = Random.Range(-_patrolDistance, _patrolDistance);

        here += new Vector3(x, 0, z);

        return here;
    }

    public void Attack()
    {
        if (!canAttack) return;

        canAttack = false;

        DOVirtual.Float(0, 1, cd, (x) => { }).OnComplete(() =>
        {
            canAttack = true;
        });

        bbPlayerRefHolder.animator.SetLayerWeight(1, 1);
        bbPlayerRefHolder.animator.SetTrigger("Attack");
    }

    public ReactionWithBall GetRandomReactionWithBall()
    {
        ReactionWithBall react = ReactionWithBall.Stay;

        int randomValue = Random.Range(0, 101);

        if (0 <= randomValue && randomValue < 45)
        {
            react = ReactionWithBall.Stay;
        }
        else if (46 <= randomValue && randomValue < 90)
        {
            react = ReactionWithBall.FightBack;
        }
        else
        {
            react = ReactionWithBall.RunAway;
        }
        return react;
    }

    // Targeted by ball
    public void StartReactionWithBall()
    {
        if (isPickedReactionWithBall) return;

        isPickedReactionWithBall = true;

        reactionWithBall = GetRandomReactionWithBall();
    }

    public void EndReactionWithBall()
    {
        isPickedReactionWithBall = false;
    }
}

public enum ReactionWithBall
{
    Stay, FightBack, RunAway
}