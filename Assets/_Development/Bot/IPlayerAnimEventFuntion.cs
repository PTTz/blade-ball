using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerAnimEventFuntion 
{
    public void OnAttackStartFuntion();

    public void OnAttackTriggerStartFuntion();

    public void OnAttackTriggerEndFuntion();

    public void OnAttackEndFuntion();
}
