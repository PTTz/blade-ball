using BehaviorDesigner.Runtime.Tasks;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBehaviorTask : Action
{
    [HideInInspector]
    public Bot bot;

    public override void OnStart()
    {
        base.OnStart();

        bot = GetComponent<Bot>();
    }
}

public class BotTaskMoveToHitBall : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.MoveToHitBall();

        return TaskStatus.Success;
    }
}

public class BotTaskAttack : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.Attack();

        return TaskStatus.Success;
    }
}

public class BotTaskStay : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot._nearEnoughToHit = 6;

        bot.bbPlayerRefHolder.navMeshAgent.isStopped = true;

        Vector3 fixPos = bot.bbPlayerRefHolder.ballParent.transform.position;

        fixPos = new Vector3(fixPos.x, 0, fixPos.z);

        bot.transform.DOLookAt(fixPos, 0.2f);

        return TaskStatus.Success;
    }
}

public class BotTaskPatrol : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.Patrol();

        return TaskStatus.Success;
    }
}

public class BotTaskStop : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.bbPlayerRefHolder.navMeshAgent.isStopped = true;

        return TaskStatus.Success;
    }
}

public class BotTaskSetDistanceToHit : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        float distance = 3;

        if (bot.bbPlayerRefHolder.ballParent.currentSpeed > 0)
        {
            distance = bot.bbPlayerRefHolder.ballParent.currentSpeed / 2f;
        }

        bot._nearEnoughToHit = distance;

        return TaskStatus.Success;
    }
}

public class BotTaskPickReactionWithBall : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.StartReactionWithBall();

        return TaskStatus.Success;
    }
}

public class BotTaskEndReactionWithBall : BotBehaviorTask
{
    public override TaskStatus OnUpdate()
    {
        bot.EndReactionWithBall();

        return TaskStatus.Success;
    }
}