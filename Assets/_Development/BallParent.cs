using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallParent : MonoBehaviour
{
    public GameObject objHitPlayerEffect;

    public Ball ball;

    public Transform tfCenter;

    public Collider colliderHitBox;

    [DisableInEditorMode]
    public Transform tfTarget;

    public Rigidbody rb;

    public float startSpeed;

    public float bonusSpeedPerHit;

    [DisableInEditorMode]
    public float currentSpeed;

    public Transform tfParent;

    public Transform tfCore;

    public AnimationCurve curveCoreUp;

    public AnimationCurve curveCoreDown;

    [DisableInEditorMode]
    public bool moving;

    private void Start()
    {
        currentSpeed = 0;

        LevelController.Ins.OnBallReseted += OnBallReseted;
    }

    private void FixedUpdate()
    {
        if (moving)
        {
            Vector3 direction = tfTarget.position - rb.position;

            direction.Normalize();

            rb.velocity = direction * currentSpeed;
        }
    }

    void OnBallReseted()
    {
        currentSpeed = 0;

        Instantiate(objHitPlayerEffect, ball.transform.position, ball.transform.rotation);

        moving = false;

        rb.velocity = Vector3.zero;

        colliderHitBox.enabled = false;

        transform.DOScale(0, 0.5f).OnComplete(() =>
        {
            transform.position = tfCenter.position;

            transform.DOScale(1, 0.5f).OnComplete(() =>
            {
                colliderHitBox.enabled = true;

                LevelController.Ins.OnBallResetDone?.Invoke();

            });
        });
    }

    public void Lauch(Transform target)
    {
        tfTarget = target;

        LevelController.Ins.OnBallTraceNewTarget?.Invoke(target);

        Lauch();
    }

    [Button]
    public void Lauch()
    {
        float distance = Vector3.Distance(transform.position, tfTarget.position);

        if (currentSpeed == 0)
        {
            currentSpeed += startSpeed;
        }
        else
        {
            currentSpeed += bonusSpeedPerHit;
        }

        float timeToTarget = (distance / currentSpeed) * 0.8f;

        float horOffset = Random.Range(-3, 3);

        float verOffset = (distance / 2f) * 0.25f;

        DOTween.Kill($"{GetInstanceID()}1");
        DOTween.Kill($"{GetInstanceID()}2");
        DOTween.Kill($"{GetInstanceID()}3");
        DOTween.Kill($"{GetInstanceID()}4");

        tfCore.DOLocalMoveY(verOffset, timeToTarget * 0.5f).SetEase(curveCoreUp).SetId($"{GetInstanceID()}1").OnComplete(() =>
        {
            tfCore.DOLocalMoveY(0, timeToTarget * 0.75f).SetId($"{GetInstanceID()}2").SetEase(curveCoreDown);
        });

        tfCore.DOLocalMoveX(horOffset, timeToTarget * 0.5f).SetId($"{GetInstanceID()}3").SetEase(curveCoreUp).OnComplete(() =>
        {
            tfCore.DOLocalMoveX(0, timeToTarget * 0.75f).SetId($"{GetInstanceID()}4").SetEase(curveCoreDown);
        });

        moving = true;
    }

    public void ResetSpeed()
    {
        currentSpeed = 0;

        ball.MaterialTo(ball.matGreen);
    }
}
