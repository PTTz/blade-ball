using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimClipEvent : MonoBehaviour
{
    public Action OnAttackStart;
    public Action OnAttackTriggerStart;
    public Action OnAttackTriggerEnd;
    public Action OnAttackEnd;

    void OnAttackStartFuntion()
    {
        OnAttackStart?.Invoke();
    }

    void OnAttackTriggerStartFuntion()
    {
        OnAttackTriggerStart?.Invoke();
    }

    void OnAttackTriggerEndFuntion()
    {
        OnAttackTriggerEnd?.Invoke();
    }

    void OnAttackEndFuntion()
    {
        OnAttackEnd?.Invoke();
    }
}
