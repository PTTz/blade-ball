using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootOverideB : MonoBehaviour, IRoot
{
    public void Funtion_A()
    {
        Debug.Log("FUNTION_A ROOT OVERIDE B");
    }

    public void Funtion_B()
    {
        Debug.Log("FUNTION_B ROOT OVERIDE B");
    }

    public void Funtion_C()
    {
        Debug.Log("FUNTION_C ROOT OVERIDE B");
    }
}
