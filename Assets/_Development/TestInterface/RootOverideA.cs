using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootOverideA : MonoBehaviour, IRoot
{
    public virtual void Funtion_A()
    {
        Debug.Log("FUNTION_A ROOT OVERIDE A");
    }

    public void Funtion_B()
    {
        Debug.Log("FUNTION_B ROOT OVERIDE A");
    }

    public void Funtion_C()
    {
        Debug.Log("FUNTION_C ROOT OVERIDE A");
    }
}
