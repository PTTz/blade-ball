using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootContacter : MonoBehaviour
{
    public RootOverideA a;

    public RootOverideB b;

    public IRoot I_a;

    public IRoot I_b;

    public IRoot I_A_1;

    public IRoot I_A_2;

    [Button]
    void CallFuntionA()
    {
        a.Funtion_A();
        b.Funtion_A();

        I_a = a.GetComponent<IRoot>();

        I_b = b;

        I_a.Funtion_B();
        I_b.Funtion_B();
    }
}
