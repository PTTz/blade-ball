using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BBPlayerRefHolder : MonoBehaviour
{
    public Animator animator;

    public NavMeshAgent navMeshAgent;

    public AnimClipEvent animClipEvent;

    public HitBox hitBox;

    public Collider colHitBox;

    public SphereBlockTrigger sphereBlockTrigger;

    public Collider colSphereBlockTrigger;

    public Ball ball;

    public BallParent ballParent;
}
