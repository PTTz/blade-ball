using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLoop : MonoBehaviour
{
    public Vector3 offset;

    public int time;

    Sequence seq;

    public AnimationCurve cur;

    private void Start()
    {
        seq = DOTween.Sequence();

        seq.Insert(0, transform.DOMove(transform.position + offset, time));

        //seq.Insert(time, transform.DOMove(transform.position - offset, time));

        seq.SetLoops(-1,LoopType.Yoyo).SetUpdate(true).SetEase(cur);

        seq.Play();
    }
}
