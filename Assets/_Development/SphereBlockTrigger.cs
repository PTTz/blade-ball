using DG.Tweening;
using MalbersAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereBlockTrigger : MonoBehaviour
{
    [SerializeField] private GameObject _objContactPaticle;

    [SerializeField] private ParticleSystem _particleSystemHitEffect;

    [SerializeField] private BBPlayerRefHolder bbPlayerRefHolder;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))
        {
            Instantiate(_objContactPaticle, other.ClosestPointOnBounds(transform.position),transform.rotation);

            bbPlayerRefHolder.ball.Hited(bbPlayerRefHolder.transform);

            bbPlayerRefHolder.colHitBox.enabled = false;

            DOVirtual.Float(0, 1, 0.5f, (x) => { }).OnComplete(() =>
            {
                bbPlayerRefHolder.colHitBox.enabled = true;
            });

            //_particleSystemHitEffect.gameObject.SetActive(true);
            //_particleSystemHitEffect.Stop();
            //_particleSystemHitEffect.Play();
        }
    }
}
