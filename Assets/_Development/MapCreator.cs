using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreator : MonoBehaviour
{
    [SerializeField] private GameObject[] objs;

    [SerializeField] private Vector3 spacing;

    [Button]
    void SetPos()
    {
        Vector3 root = objs[0].transform.position;

        for (int i = 1; i < objs.Length; i++)
        {
            objs[i].transform.position = root + spacing * i;
        }
    }
}
