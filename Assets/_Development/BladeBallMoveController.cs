using DG.Tweening;
using MalbersAnimations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BladeBallMoveController : MonoBehaviour
{
    public float cd;

    bool canAttack = true;

    [SerializeField] private BBPlayerRefHolder playerRefHolder;

    private void Start()
    {
        LevelController.Ins.OnPlayerImmuteChangeTo += OnPlayerImmuteChangeTo;
    }

    void OnPlayerImmuteChangeTo(bool value)
    {
        playerRefHolder.hitBox.SetCanTakeDmg(!value);
    }

    public void OnClickBtnBlock()
    {
        if (playerRefHolder == null || playerRefHolder.IsDestroyed()) return;

        if (!canAttack) return;

        canAttack = false;

        DOVirtual.Float(0, 1, cd, (x) => { }).OnComplete(() =>
        {
            canAttack = true;
        });

        playerRefHolder.animator.SetLayerWeight(1, 1);
        playerRefHolder.animator.SetTrigger("Attack");
    }

    public void OnJoyStickDrag(Vector2 value)
    {
        if (playerRefHolder == null || playerRefHolder.IsDestroyed()) return;

        playerRefHolder.animator.SetFloat("InputHorizontal", value.x);

        playerRefHolder.animator.SetFloat("InputVertical", value.y);

        playerRefHolder.animator.SetFloat("InputMagnitude", value.magnitude);
    }

    public void OnJoyStickUp()
    {
        if (playerRefHolder == null || playerRefHolder.IsDestroyed()) return;

        float currentInputMagnitude = playerRefHolder.animator.GetFloat("InputMagnitude");

        DOVirtual.Float(currentInputMagnitude, 0, 0.2f, (x) =>
        {
            playerRefHolder.animator.SetFloat("InputMagnitude", x);
        });
    }
}
