using MalbersAnimations;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasGameplayUI : MonoBehaviour
{
    public BBPlayerRefHolder player;

    //--------------------- CanvasElement ----------

    public Button btnChangePlayerImmute;

    public TextMeshProUGUI tmpPlayerImmute;

    public Button btnReset;

    public TextMeshProUGUI tmpPlayerLess;

    public Button btnAttack;

    public MobileJoystick mobieJoyStick;

    //---------------------- Action -----------------

    public Action OnClickBtnChangePlayerImmute;

    public Action OnClickBtnAttack;

    public Action OnJoyStickBtnDown;

    public Action<Vector2> OnDragJoyStick;

    public Action OnJoyStickBtnUp;

    private void Start()
    {
        btnReset.onClick.AddListener(OnClickBtnReset);

        btnChangePlayerImmute.onClick.AddListener(OnClickBtnChangePlayerImmuteFuntion);

        btnAttack.onClick.AddListener(OnClickBtnAttackFuntion);

        mobieJoyStick.OnAxisChange.AddListener(OnDragJoyStickFuntion);

        mobieJoyStick.OnJoystickUp.AddListener(OnJoyStickBtnUpFuntion);

        btnReset.gameObject.SetActive(false);

        LevelController.Ins.OnPlayerDie += OnPlayerDie;

        LevelController.Ins.OnAmountPlasyerLessChangeTo = OnAmountPlayerLessChangeTo;

        LevelController.Ins.OnPlayerImmuteChangeTo += OnPlayerImmuteChangeTo;

        OnClickBtnChangePlayerImmute += LevelController.Ins.OnClickBtnChangePlayerImmute;

        //---------------------------
    }

    void OnClickBtnAttackFuntion()
    {
        OnClickBtnAttack?.Invoke();
    }

    void OnJoyStickBtnUpFuntion()
    {
        OnJoyStickBtnUp?.Invoke();
    }

    void OnDragJoyStickFuntion(Vector2 input)
    {
        OnDragJoyStick?.Invoke(input);
    }

    void OnPlayerDie()
    {
        btnReset.gameObject.SetActive(true);
    }

    public void OnClickBtnReset()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickBtnChangePlayerImmuteFuntion()
    {
        OnClickBtnChangePlayerImmute?.Invoke();
    }

    public void OnPlayerImmuteChangeTo(bool value)
    {
        tmpPlayerImmute.text = $"Player Immute : {value}";
    }

    public void OnAmountPlayerLessChangeTo(int amount)
    {
        if (amount > 1)
        {
            tmpPlayerLess.text = $"Player : {amount}";
        }
        else if (amount == 1)
        {
            btnReset.gameObject.SetActive(true);
        }
    }
}
