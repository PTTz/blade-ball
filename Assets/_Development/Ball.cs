using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public BallParent ballParent;

    [DisableInEditorMode]
    public Transform tfTarget;

    public MeshRenderer _meshRenderer;

    [PreviewField]
    public Material matRed;

    [PreviewField]
    public Material matGreen;

    [Button]
    public void Hited(Transform other)
    {
        tfTarget = LevelController.Ins.GetTarget(transform, other);

        if (tfTarget == LevelController.Ins.player)
        {
            MaterialTo(matRed);
        }
        else
        {
            MaterialTo(matGreen);
        }

        ballParent.Lauch(tfTarget);

        LevelController.Ins.OnBallHited?.Invoke();
    }

    public void MaterialTo(Material material)
    {
        var matArrays = _meshRenderer.materials;

        matArrays = new Material[matArrays.Count()];

        for (int i = 0; i < matArrays.Count(); i++)
        {
            matArrays[i] = material;
        }

        _meshRenderer.materials = matArrays;
    }
}
